package com.itchtector.itchtector_alpha_v2.helper

import android.content.Context
import com.itchtector.itchtector_alpha_v2.constant.S

object SessionHelper {
    val KEY_SIGN_IN_PROVIDER = "signInProvider"

    fun putPreference(key: String, value: String) {
        PreferenceHelper.putKeyValue(key, value, S.SESSION_PREF_NAME)
    }

    fun getPreference(key: String): String? {
        return PreferenceHelper.getStringValue(key, S.SESSION_PREF_NAME)
    }

    fun clear(context: Context) {
        val editor = PreferenceHelper.getPreferencesSession()!!.edit()
        editor.clear()
        editor.apply()
    }

    fun getSignInProvider() {
        getPreference(KEY_SIGN_IN_PROVIDER)
    }

    fun isRefreshTokenExists(): Boolean {
        val refreshToken = getPreference(S.REFRESH_TOKEN)
        return refreshToken != null && refreshToken != ""
    }

    fun putRefreshToken(token: String) {
        putPreference(S.REFRESH_TOKEN, token)
        //        putPreference(KEY_SIGN_IN_PROVIDER, provider);
    }

    fun getRefreshToken(): String? {
        return getPreference(S.REFRESH_TOKEN)
    }

    fun removeRefreshToken() {
        PreferenceHelper.removeKeyValue(S.REFRESH_TOKEN, S.SESSION_PREF_NAME)
    }

    fun putAccessToken(token: String) {
        putPreference(S.ACCESS_TOKEN, token)
    }

    fun getAccessToken(): String? {
        return getPreference(S.ACCESS_TOKEN)
    }

    fun removeAccessToken() {
        PreferenceHelper.removeKeyValue(S.ACCESS_TOKEN, S.SESSION_PREF_NAME)
    }

    fun isUserKeyExists(): Boolean {
        val userKey = getPreference(S.USER_KEY)
        return userKey != null && userKey != ""
    }

    fun putUserKey(userKey: String) {
        putPreference(S.USER_KEY, userKey)
    }

    fun getUserKey(): String? {
        return getPreference(S.USER_KEY)
    }

    fun removeUserKey() {
        PreferenceHelper.removeKeyValue(S.USER_KEY, S.SESSION_PREF_NAME)
    }
}