package com.itchtector.itchtector_alpha_v2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.itchtector.itchtector_alpha_v2.constant.RC
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.databinding.SurveyActivityBinding

class SurveyActivity : AppCompatActivity() {
    private var surveyIndex: Int = 0
    private var surveyId: String? = null
    private var binding: SurveyActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.survey_activity)
        surveyIndex = intent.getIntExtra(S.SURVEY_INDEX, 0)
        surveyId = intent.getStringExtra(S.SURVEY_ID)

        setUpToolBar()

        val bundle = Bundle()
        bundle.putInt(S.SURVEY_INDEX, surveyIndex)
        if (surveyId != null) {
            bundle.putString(S.SURVEY_ID, surveyId)
        }
        val surveyActivityFragment = SurveyActivityFragment()
        surveyActivityFragment.arguments = bundle

        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, surveyActivityFragment)
        transaction.commit()
    }

    override fun onBackPressed() {
        if (surveyIndex == RC.SURVEY_POST_1) {
            return
        }

        if (supportFragmentManager.backStackEntryCount > 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
            finish()
            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpToolBar() {
        val toolbar = binding!!.toolbar
        setSupportActionBar(toolbar)

        if (surveyIndex != RC.SURVEY_POST_1) {
            supportActionBar?.setHomeButtonEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationIcon(R.mipmap.icon_menu)
        }
        else {
            supportActionBar?.setHomeButtonEnabled(true)
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            toolbar.setNavigationIcon(null)
        }

        var title = ""
        if (surveyIndex == RC.SURVEY_PRE_1 || surveyIndex == RC.SURVEY_PRE_2) {
            title = "자기 전 기록"
        } else {
            title = "일어난 후 기록"
        }

        supportActionBar?.title = title
    }
}