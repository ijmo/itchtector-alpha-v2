package com.itchtector.itchtector_alpha_v2.bus

import com.jakewharton.rxrelay2.PublishRelay
import com.jakewharton.rxrelay2.Relay
import io.reactivex.Observable

class RxBus private constructor() {
    private var bus: Relay<Any>? = null

    init {
        bus = PublishRelay.create<Any>().toSerialized()
    }

    fun send(event: Any) {
        bus!!.accept(event)
    }

    fun <T> toObservable(eventType: Class<T>): Observable<T> {
        return bus!!.ofType(eventType)
    }

    fun hasObservers(): Boolean {
        return bus!!.hasObservers()
    }

    companion object {
        private var instance: RxBus? = null

        fun getInstance(): RxBus? {
            if (instance == null) {
                synchronized(RxBus::class.java) {
                    if (instance == null) {
                        instance = RxBus()
                    }
                }
            }
            return instance
        }
    }
}