package com.itchtector.itchtector_alpha_v2.db.model

import android.os.Build
import androidx.databinding.ObservableField
import bolts.Task
import com.crashlytics.android.Crashlytics
import com.itchtector.itchtector_alpha_v2.constant.DataKind
import com.itchtector.itchtector_alpha_v2.constant.DeviceStatus
import com.itchtector.itchtector_alpha_v2.R
import com.itchtector.itchtector_alpha_v2.bus.RxBus
import com.itchtector.itchtector_alpha_v2.constant.C
import com.itchtector.itchtector_alpha_v2.helper.SessionHelper
import com.itchtector.itchtector_alpha_v2.bus.message.NewFile
import com.itchtector.itchtector_alpha_v2.bus.message.RawData
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.DateUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import com.mbientlab.metawear.Data
import com.mbientlab.metawear.DeviceInformation
import com.mbientlab.metawear.MetaWearBoard
import com.mbientlab.metawear.builder.RouteComponent
import com.mbientlab.metawear.data.Acceleration
import com.mbientlab.metawear.data.EulerAngles
import com.mbientlab.metawear.module.Led
import com.mbientlab.metawear.module.MagnetometerBmm150
import com.mbientlab.metawear.module.SensorFusionBosch
import com.mbientlab.metawear.module.Settings
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import java.util.concurrent.TimeUnit

open class Bracelet(@PrimaryKey open var index: Int = -1,
                    @Ignore val metaWearBoard: MetaWearBoard? = null) : RealmObject() {
    var macAddress: String = ""
        set(value) {
            field = value
            macAddressShort = value.replace(":", "")
        }

    @Ignore private lateinit var TAG: String
    @Ignore var macAddressShort: String = ""
//    @Ignore var metaWearBoard: MetaWearBoard? = null
    @Ignore val statusOb: ObservableField<String> = ObservableField()
    @Ignore val imageOb: ObservableField<Int> = ObservableField()
    @Ignore val compositeDisposable = CompositeDisposable()
    @Ignore var disposable: Disposable? = null
    @Ignore var surveyId: String? = null
    @Ignore var currentFileState: FileState? = null
    @Ignore private var isRunning: Boolean = false
    @Ignore var isBatteryOk: Boolean = true
    @Ignore var batteryChargedOb: ObservableField<String> = ObservableField()
    @Ignore var status = DeviceStatus.DISCONNECTED
        set(value) {
            field = value

            when(field) {
                DeviceStatus.CONNECTING -> {
                    statusOb.set("연결 중")
                    when(index) {
                        0 -> imageOb.set(R.mipmap.sensor_error)
                        1 -> imageOb.set(R.mipmap.sensor_error_right)
                    }
                }
                DeviceStatus.CONNECTED -> {
                    if (isBatteryOk) {
                        statusOb.set("측정 대기 중")
                        when(index) {
                            0 -> imageOb.set(R.mipmap.sensor)
                            1 -> imageOb.set(R.mipmap.sensor_right)
                        }
                    } else {
                        statusOb.set("배터리를 충전해 주세요.")
                        when(index) {
                            0 -> imageOb.set(R.mipmap.sensor_error)
                            1 -> imageOb.set(R.mipmap.sensor_error_right)
                        }
                    }
                }
                DeviceStatus.DISCONNECTING -> {
                    statusOb.set("연결 해제 중")
                    when(index) {
                        0 -> imageOb.set(R.mipmap.sensor_error)
                        1 -> imageOb.set(R.mipmap.sensor_error_right)
                    }
                }
                DeviceStatus.DISCONNECTED -> {
                    statusOb.set("장치를 연결해 주세요.")
                    when(index) {
                        0 -> imageOb.set(R.mipmap.sensor_none)
                        1 -> imageOb.set(R.mipmap.sensor_none_right)
                    }
                }
                DeviceStatus.RUNNING -> {
                    if (isBatteryOk) {
                        statusOb.set("측정 중")
                        when(index) {
                            0 -> imageOb.set(R.mipmap.sensor)
                            1 -> imageOb.set(R.mipmap.sensor_right)
                        }
                    } else {
                        statusOb.set("배터리를 충전해 주세요.")
                        when(index) {
                            0 -> imageOb.set(R.mipmap.sensor_error)
                            1 -> imageOb.set(R.mipmap.sensor_error_right)
                        }
                    }
                }
            }

            RxBus.getInstance()!!.send(field)
        }

    init {
        this.status = DeviceStatus.DISCONNECTED
        batteryChargedOb.set("-")
        if (metaWearBoard != null) {
            macAddress = metaWearBoard.macAddress
            TAG = getLRBracket() + " " + metaWearBoard.macAddress
            disposable = getMainLooper().subscribe()
        }
    }

    fun connect(): Task<Void>? {
        metaWearBoard?.onUnexpectedDisconnect { statusNo ->
            handleError(Throwable("Unexpectedly lost connection: $statusNo"))
            status = DeviceStatus.DISCONNECTED
            reconnect()
        }
        status = DeviceStatus.CONNECTING
        return metaWearBoard?.connectAsync()?.continueWith { task: Task<Void> ->
            if (task.isFaulted) {
                handleError(Throwable("Failed to connect."))
                reconnect()
            } else {
                LogUtil.i(TAG, "Connected to $macAddress")
                status = DeviceStatus.CONNECTED

                val settings = metaWearBoard.getModule(Settings::class.java)!!
                settings.editBleConnParams()
                    .maxConnectionInterval(if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) 11.25f else 7.5f)
                    .commit()

                if (isRunning && surveyId != null) {
                    stopTracking()
                    startTracking(surveyId!!)
                } else {
                    initSensorRoute()
                }

                readDeviceInfo()
            }
            null
        }
    }

    fun reconnect() {
        if (metaWearBoard?.isConnected!!) {
            disconnect()?.continueWith<Void> { task: Task<Void> ->
                connect()
                null
            }
        } else {
            connect()
        }
    }

    fun disconnect(): Task<Void>? {
        status = DeviceStatus.DISCONNECTING
        return metaWearBoard?.disconnectAsync()?.let { task ->
            stopLed()
            task.continueWith<Void> { t ->
                status = DeviceStatus.DISCONNECTED
                null
            }
            null
        }
    }

    private fun initSensorRoute() {
        metaWearBoard?.tearDown()

        val sensorFusion = metaWearBoard?.getModule(SensorFusionBosch::class.java)
        sensorFusion!!.configure().
            mode(SensorFusionBosch.Mode.NDOF).
            accRange(SensorFusionBosch.AccRange.AR_16G).
            gyroRange(SensorFusionBosch.GyroRange.GR_2000DPS).
            commit()
        sensorFusion.stop()

        val magnetometer = metaWearBoard?.getModule(MagnetometerBmm150::class.java)
        magnetometer!!.usePreset(MagnetometerBmm150.Preset.HIGH_ACCURACY)
        magnetometer.configure().
            outputDataRate(MagnetometerBmm150.OutputDataRate.ODR_30_HZ).
            zReps(256.toShort()).
            xyReps(511.toShort()).
            commit()

        val settings = metaWearBoard?.getModule(Settings::class.java)!!
        settings.battery().addRouteAsync { source: RouteComponent ->
            source.stream { data: Data, env: Any ->
                val subject = "BA"
                val bs = data.value(Settings.BatteryState::class.java)
                batteryChargedOb.set(bs.charge.toString())
                timeAfterLastBatteryRx = 0

                // Write into file
                val objects =
                    arrayOf(subject,
                        batteryChargedOb.get()!!,
                        0,
                        0,
                        0,
                        0,
                        DateUtil.getTimeNow().time.toString(),
                        0)
                val row = AppUtil.stringJoin(",", objects, "\n")
                LogUtil.v(TAG, row)
                RxBus.getInstance()!!.send(RawData(index, subject, row))
            }
        }

        sensorFusion.correctedAngularVelocity().
            addRouteAsync { source -> source.stream { data, env ->
                val subject = "GY" // Gyroscope
                val v = data.value(SensorFusionBosch.CorrectedAngularVelocity::class.java)

                val objects =
                    arrayOf(subject,
                        v.x(),
                        v.y(),
                        v.z(),
                        0,
                        0,
                        data.timestamp().timeInMillis,
                        v.accuracy())
                val row = AppUtil.stringJoin(",", objects, "\n")
                RxBus.getInstance()!!.send(RawData(index, subject, row))
            } }

        sensorFusion.eulerAngles().
            addRouteAsync { source -> source.stream { data, env ->
                val subject = "OR" // Orientation
                val v = data.value(EulerAngles::class.java)
                val objects =
                    arrayOf(subject,
                        v.heading(),
                        v.pitch(),
                        v.roll(),
                        v.yaw(),
                        0,
                        data.timestamp().timeInMillis,
                        3)
                val row = AppUtil.stringJoin(",", objects, "\n")
                RxBus.getInstance()!!.send(RawData(index, subject, row))
            } }

        sensorFusion.linearAcceleration().
            addRouteAsync { source -> source.stream { data, env ->
                val subject = "LI" // Linear accelerometer
                val v = data.value(Acceleration::class.java)
                val objects = arrayOf(subject,
                    v.x(),
                    v.y(),
                    v.z(),
                    0,
                    0,
                    data.timestamp().timeInMillis,
                    3)
                val row = AppUtil.stringJoin(",", objects, "\n")
                RxBus.getInstance()!!.send(RawData(index, subject, row))
            } }
    }

    fun startTracking(surveyId: String) {
        if (metaWearBoard != null
            && metaWearBoard.isConnected) {
            if (isBatteryOk) {
                initSensorRoute()
                val sensorFusion = metaWearBoard.getModule(SensorFusionBosch::class.java)
                sensorFusion.linearAcceleration().start()
                sensorFusion.correctedAngularVelocity().start()
                sensorFusion.eulerAngles().start()
                sensorFusion.start()
                status = DeviceStatus.RUNNING
                this.surveyId = surveyId
                ledTick = 500
                ledDuration = 10000
                compositeDisposable.clear()
                compositeDisposable.add(getDataSubscriber())
            }
            isRunning = true
        }
    }

    fun stopTracking() {
        if (metaWearBoard != null && metaWearBoard.isConnected) {
            val sensorFusion = metaWearBoard.getModule(SensorFusionBosch::class.java)
            sensorFusion.stop()
            sensorFusion.eulerAngles().stop()
            sensorFusion.correctedAngularVelocity().stop()
            sensorFusion.linearAcceleration().stop()
            status = DeviceStatus.CONNECTED
            ledTick = 0
            ledDuration = 0
            isRunning = false
            compositeDisposable.clear()
            metaWearBoard.tearDown()

            if (currentFileState != null && currentFileState?.size!! > 0) {
                currentFileState?.closeFileOutputStreams()
                sendFile(currentFileState?.id!!)
                currentFileState = null
            }
        }
    }

    fun disposeAll() {
        disposable?.dispose()
        compositeDisposable.dispose()
    }

    @Ignore private var timeAfterTryToReadBattery = C.TICK_BATTERY_CHECK
    @Ignore private var timeAfterLastBatteryRx: Long = 0
    @Ignore private var timeAfterLastSensorRx: Long = 0
    @Ignore private var timeAfterTryToPlayLed: Long = 0
    @Ignore var ledTick: Long = 0
    @Ignore var ledDuration: Long = 0

    private fun getMainLooper(): Observable<Long> {
        return Observable
            .interval(C.DEVICE_LOOPER_INTERVAL, TimeUnit.MILLISECONDS)
            .doOnDispose {
                LogUtil.e(TAG, "mainLooper is disposed.")
            }.observeOn(AndroidSchedulers.mainThread())
            .filter { metaWearBoard?.isConnected!! }
            .doOnNext {
                timeAfterTryToReadBattery += C.DEVICE_LOOPER_INTERVAL
                timeAfterLastBatteryRx += C.DEVICE_LOOPER_INTERVAL
                timeAfterLastSensorRx += C.DEVICE_LOOPER_INTERVAL
                timeAfterTryToPlayLed += C.DEVICE_LOOPER_INTERVAL

                // Read battery level
                if (timeAfterTryToReadBattery >= C.TICK_BATTERY_CHECK) {
                    timeAfterTryToReadBattery = 0
                    metaWearBoard?.readBatteryLevelAsync()?.continueWith { task: Task<Byte> ->
                        timeAfterLastBatteryRx = 0
                        val batteryCharge = task.result.toInt()
                        batteryChargedOb.set("$batteryCharge%")
                        LogUtil.v(TAG, "${getLRBracket()} BATTERY: $batteryCharge")

                        if (isBatteryOk && batteryCharge <= 5) {
                            isBatteryOk = false
                            status = status
                            if (isRunning) {
                                stopTracking()
                            }
                        } else if (!isBatteryOk && batteryCharge >= 50) {
                            isBatteryOk = true
                            status = status
                            if (isRunning && surveyId != null) {
                                startTracking(surveyId!!)
                            }
                        }

                        val subject = "BA" // Battery
                        val row = "BA,$batteryCharge,0,0,0,0,${System.currentTimeMillis()},0\n"
                        RxBus.getInstance()!!.send(RawData(index, subject, row))

                        null
                    }
                }

                // Play LED
                if (timeAfterTryToPlayLed >= C.TICK_PLAY_LED) {
                    ledDuration -= timeAfterTryToPlayLed
                    timeAfterTryToPlayLed = 0

                    if (ledDuration >= 0) {
                        if (ledDuration % ledTick == 0L) {
                            playLed(Led.PatternPreset.BLINK)
                        }
                    } else {
                        if (ledDuration < -C.TICK_PLAY_LED_HEARTBEAT) {
//                            LogUtil.v(TAG, "ledDuration=" + ledDuration + ", status=" + status.name)
                            ledDuration = 0
                            playLed(null)
                        }
                    }
                }
            }
    }

    private fun getDataSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(RawData::class.java)
            .filter { rawData -> rawData.index == index }
            .buffer(C.DATA_PROCESSOR_TIMESPAN, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.newThread())
            .map { arr: List<RawData> ->
                if (arr.isEmpty()) {
                    "${getLRBracket()} Passing: 0 row"
                } else {
                    prepareFileState()

                    if (currentFileState == null) {
                        throw Throwable("currentFileState is null")
                    }

                    if (currentFileState?.isWritable()!!) {
                        val chunk = AppUtil.stringJoin("", arr.map { it.message }.toTypedArray(), "")
                        val chunkBytes = chunk.toByteArray()
                        val sizeGyro = arr.filter { it.kind == "GY" }.size
                        val sizeOri = arr.filter { it.kind == "OR" }.size
                        val sizeLin = arr.filter { it.kind == "LI" }.size
                        currentFileState?.write(chunkBytes)
                        "${getLRBracket()} Recv: ${arr.size}($sizeGyro,$sizeOri,$sizeLin) during ${C.DATA_PROCESSOR_TIMESPAN}ms --> ${currentFileState?.filename}"
                    } else {
                        throw Throwable("file is not writable")
                    }
                }
            }.subscribe({chunk -> LogUtil.v(TAG, chunk)}, {e -> handleError(e)})
    }

    @Synchronized private fun prepareFileState() {
        if (currentFileState != null && currentFileState?.size!! > C.MAX_FILE_SIZE) {
            currentFileState?.closeFileOutputStreams()
            sendFile(currentFileState?.id!!)
            currentFileState = null
        }

        if (currentFileState == null || !currentFileState?.isWritable()!!) {
            val userKey= SessionHelper.getUserKey()!!
            val dirPath = userKey + "/" + DateUtil.getStringDateFormat("yyyy/MM/dd")
            val filename = (DateUtil.getStringDateFormat("yyyyMMdd_HHmmss_SSS")
                    + "_" + getLR()
                    + "_" + macAddressShort + ".csv")

            val newFileState = FileState(DataKind.META_MOTION_R.toString(), index, surveyId!!, userKey, dirPath, filename)
//            LogUtil.d(TAG, newFileState.getId() + " - WRITING");
//            LogUtil.v(TAG, String.format(Locale.US, "%s %s %s %s", (newFileState != null? "exists":"none!")
//                    , newFileState.getUserKey(), newFileState.getDirPath(), newFileState.getFilename()));

            if (newFileState.makeDir()) {
                newFileState.openFileOutputStreams()
                currentFileState = newFileState
            } else {
                newFileState.closeFileOutputStreams()
            }

            try {
                Realm.getDefaultInstance().use { realm ->
                    realm.beginTransaction()
                    realm.copyToRealm(newFileState)
                    realm.commitTransaction()
                }
            } catch (e: Throwable) {
                handleError(e)
            }
        }
    }

    private fun sendFile(fileStateId: String) {
        RxBus.getInstance()!!.send(NewFile(fileStateId))
    }

    fun getLR(): String {
        return (if (index == 0) "L" else "R")
    }

    fun getLRBracket(): String {
        return (if (index == 0) "[L]" else "[R]")
    }

    private fun playLed(givenPattern: Led.PatternPreset?) {
        if (status == DeviceStatus.DISCONNECTING || status == DeviceStatus.DISCONNECTED) {
            return
        }

        val highIntensity: Byte = when (status) {
            DeviceStatus.RUNNING -> 5
            else -> 31
        }
        val color: Led.Color = when (status) {
            DeviceStatus.RUNNING -> Led.Color.BLUE
            else -> Led.Color.GREEN
        }
        val pattern: Led.PatternPreset = givenPattern ?: when (status) {
            DeviceStatus.RUNNING -> Led.PatternPreset.BLINK
            else -> Led.PatternPreset.PULSE
        }

        metaWearBoard?.getModule(Led::class.java)?.let { led ->
            led.stop(true)
            led.editPattern(color, pattern).repeatCount(1.toByte())
                .highIntensity(highIntensity)
                .commit()
            led.play()
        }
    }

    private fun stopLed() {
        metaWearBoard?.getModule(Led::class.java)?.let { led ->
            led.pause()
            led.stop(true)
        }
    }

    private fun readDeviceInfo() {
        metaWearBoard?.readDeviceInformationAsync()?.
            continueWith { task: Task<DeviceInformation> ->
                LogUtil.i(TAG, "${task.result}")
                null
            }
    }

    private fun handleError(tag: String, e: Throwable) {
        var msg = e.message?.replace(",", ".")
        msg = msg ?: e.toString()
        val objects = arrayOf(
            "ER",
            e.javaClass.simpleName,
            msg,
            0,
            0,
            0,
            DateUtil.getTimeNow().time.toString(),
            0
        )
        val row = AppUtil.stringJoin(",", objects, "\n")
        RxBus.getInstance()?.send(RawData(index, "", row))
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}
