package com.itchtector.itchtector_alpha_v2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.crashlytics.android.Crashlytics
import com.firebase.ui.auth.AuthUI
import com.firebase.ui.auth.ErrorCodes
import com.firebase.ui.auth.IdpResponse
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.itchtector.itchtector_alpha_v2.constant.RC
import com.itchtector.itchtector_alpha_v2.databinding.LoginActivityBinding
import com.itchtector.itchtector_alpha_v2.helper.AuthHelper
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import java.lang.IllegalArgumentException

class LoginActivity : AppCompatActivity() {
    private val TAG = LoginActivity::class.java.simpleName
    private var binding: LoginActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LogUtil.i(TAG, "onCreate $savedInstanceState")

        binding = DataBindingUtil.setContentView(this, R.layout.login_activity)

        binding!!.btnContinue.setOnClickListener { v ->
            checkEmailVerified()
        }

        binding!!.btnSendEmail.setOnClickListener { v ->
            FirebaseAuth.getInstance().currentUser!!.sendEmailVerification()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        showSnackbar("메일 전송 요청하였습니다.${task.isSuccessful}", Snackbar.LENGTH_SHORT)
                    } else {
                        showSnackbar("오류가 있습니다.${task.exception!!.message}", Snackbar.LENGTH_SHORT)
                    }
                }
        }

        binding!!.btnLogin.setOnClickListener { v ->
            startAuthUI()
        }

        showSplash()

        val (refreshToken, _) = AuthHelper.getTokenPair()
        LogUtil.i(TAG, "refreshToken=$refreshToken")
        if (refreshToken != null) {
            startMainActivity()
        } else {
            checkIdToken()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onResume() {
        super.onResume()
//        binding?.btnContinue?.isEnabled = false
//        checkEmailVerified{ binding?.btnContinue?.isEnabled = true }
    }

    override fun onPause() {
        super.onPause()
    }

    @SuppressLint("CheckResult")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        LogUtil.d(TAG, "$requestCode, $resultCode")

        if (requestCode == RC.SIGN_IN) {
            val response = IdpResponse.fromResultIntent(data)
            if (resultCode == RESULT_OK) {
                checkIdToken()
            } else {
                when {
                    response == null -> {
                        LogUtil.d(TAG, "Sign-in Failed: Backbutton pressed")
                        finishAffinity()
                        System.exit(0)
                    }
                    response.error!!.errorCode == ErrorCodes.NO_NETWORK -> {
                        LogUtil.d(TAG, "Sign-in Failed: " + response.error!!.message)
                        showSnackbar("Cannot Connect to Internet", Snackbar.LENGTH_SHORT)
                    }
                    else -> {
                        LogUtil.d(TAG, "Unknown Error!")
                        showSnackbar("인증에 실패하였습니다.", Snackbar.LENGTH_SHORT)
                    }
                }
            }
        }
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
        finishAfterTransition()
    }

    private fun startAuthUI() {
        showSplash()
        startActivityForResult(
            AuthUI.getInstance()
                .createSignInIntentBuilder()
                .setIsSmartLockEnabled(false)
                .setAvailableProviders(AuthHelper.providers)
                .setLogo(R.mipmap.appicon_typo)
                .setTheme(R.style.AppTheme)
                .setTosAndPrivacyPolicyUrls(
                    "https://example.com/terms.html",
                    "https://example.com/privacy.html")
                .build(),
            RC.SIGN_IN
        )
    }

    private fun showSplash() {
        binding!!.splash.visibility = View.VISIBLE
    }

    private fun hideSplash() {
        binding!!.splash.visibility = View.GONE
    }

    private fun showSnackbar(msg: String, duration: Int) {
        Snackbar.make(binding!!.snackbar, msg, duration)
            .setAction("Action", null).show()
    }

    private fun checkIdToken() {
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            user.getIdToken(true)
                .addOnCompleteListener { task ->
                    LogUtil.d("$TAG.checkIdToken()", "task.isSuccessful=${task.isSuccessful}")
                    if (task.isSuccessful) {
                        val result = task.result!!
                        val claims = result.claims
                        LogUtil.d("$TAG.checkIdToken()", "claims=${claims}")
                        if (result.signInProvider == "password"
                            && claims["email_verified"] == false) {
                            val email = claims["email"] as String
                            binding!!.textEmail.text = email
                            user.sendEmailVerification()
                            hideSplash()
                        } else {
                            startMainActivity()
                        }
                    } else {
                        LogUtil.d(TAG, "idToken not found")
                        silentSignIn()
                    }
                }
        } else {
            startAuthUI()
        }
    }

    private fun checkEmailVerified(onComplete: () -> Unit = { }) {
        val user = FirebaseAuth.getInstance().currentUser!!
        user.getIdToken(true)
            .addOnCompleteListener { task ->
                onComplete()
                if (task.isSuccessful) {
                    val result = task.result!!
                    if (result.signInProvider == "password"
                        && result.claims["email_verified"] == true) {
                        startMainActivity()
                    } else {
                        showSnackbar("아직 인증이..", Snackbar.LENGTH_SHORT)
                    }
                }
            }
    }

    private fun silentSignIn() {
        try {
            LogUtil.d(TAG, "try silentSignIn...")
            AuthUI.getInstance().silentSignIn(this, AuthHelper.providers)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        LogUtil.d(TAG, "Silent sign-in SUCCESS")
                        checkIdToken()
                    } else {
                        LogUtil.d(TAG, "Silent sign-in FAILED")
                        startAuthUI()
                    }
                }
        } catch (e: IllegalArgumentException) {
            LogUtil.d(TAG, e.message ?: "")
            startMainActivity()
        } catch (e: Throwable) {
            handleError(e)
            startAuthUI()
        }
    }

    private fun handleError(tag: String, e: Throwable) {
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}