package com.itchtector.itchtector_alpha_v2.db.model

import com.itchtector.itchtector_alpha_v2.util.DateUtil
import io.realm.RealmObject

open class SurveyEntry() : RealmObject() {
    var key: String? = null
    var value: String? = null
    var timeCreation = DateUtil.getTimeNow()

    constructor(key: String, value: String) : this() {
        this.key = key
        this.value = value
    }
}