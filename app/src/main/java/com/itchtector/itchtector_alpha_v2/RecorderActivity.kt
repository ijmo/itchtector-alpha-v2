package com.itchtector.itchtector_alpha_v2

import android.content.Context
import android.content.Intent
import android.media.AudioFormat
import android.media.AudioRecord
import android.media.MediaRecorder
import android.net.wifi.WifiManager
import android.os.Bundle
import android.os.PowerManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.crashlytics.android.Crashlytics
import com.itchtector.itchtector_alpha_v2.adapter.BraceletAdapter
import com.itchtector.itchtector_alpha_v2.bus.RxBus
import com.itchtector.itchtector_alpha_v2.bus.message.NewFile
import com.itchtector.itchtector_alpha_v2.databinding.RecorderActivityBinding
import com.itchtector.itchtector_alpha_v2.bus.message.RawData
import com.itchtector.itchtector_alpha_v2.bus.message.UnsentCompleteSignal
import com.itchtector.itchtector_alpha_v2.constant.*
import com.itchtector.itchtector_alpha_v2.db.model.FileState
import com.itchtector.itchtector_alpha_v2.db.model.Survey
import com.itchtector.itchtector_alpha_v2.helper.SessionHelper
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.DateUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.*
import java.util.concurrent.TimeUnit

class RecorderActivity : AppCompatActivity() {
    private val TAG = RecorderActivity::class.simpleName!!
    private val compositeDisposable = CompositeDisposable()
    private var binding: RecorderActivityBinding? = null
    private var braceletAdapter: BraceletAdapter? = null
    private var surveyIndex: Int = 0
    private var surveyId: String? = null
    private var isStarted: Boolean = false
    private var isRecording: Boolean = false
    private var wakeLock: PowerManager.WakeLock? = null
    private var wifiLock: WifiManager.WifiLock? = null
    private var currentFileState: FileState? = null

    private var recorder: AudioRecord? = null
    private var audioSource: Int = MediaRecorder.AudioSource.MIC
    private var sampleRate: Int = 16000
    private var channelCount: Int = AudioFormat.CHANNEL_IN_MONO
    private var audioFormat: Int = AudioFormat.ENCODING_PCM_16BIT
    private var bufferSize: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding =
            DataBindingUtil.setContentView(this, R.layout.recorder_activity)
        braceletAdapter = BraceletAdapter()
        braceletAdapter!!.itemListener = { i: Int -> onClickBraceletItem(i) }

        binding!!.braceletMap = BraceletKeeper.braceletMap
        binding!!.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding!!.recyclerView.adapter = braceletAdapter

        surveyIndex = intent.getIntExtra(S.SURVEY_INDEX, 0)
        surveyId = intent.getStringExtra(S.SURVEY_ID)

        setUpToolbar()

        binding!!.btnContinue.setOnClickListener { v ->
            if (!isStarted) {
                isStarted = true
                startTracking()
            } else {
                MaterialDialog(this)
                    .title(R.string.want_to_finish)
                    .message(R.string.finish_if_you_want)
                    .positiveButton(R.string.finish_tracking) { dialog ->
                        stopTracking(true, false)
                        moveToSurvey()
                    }
                    .negativeButton(R.string.continue_tracking)
                    .show()
            }
        }

        prepareRecording()

        binding!!.btnDevStart.hide()
    }

    override fun onBackPressed() {
        MaterialDialog(this)
            .title(R.string.want_to_cancel)
            .message(R.string.cancel_if_you_want)
            .positiveButton(R.string.cancel_tracking) { dialog ->
                stopTracking(false, true)
                backToMain()
            }
            .negativeButton(R.string.continue_tracking)
            .show()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun setUpToolbar() {
        val toolbar = binding!!.toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = "가려움 측정"
    }

    private fun startTracking() {
        wakeLock =
            (getSystemService(Context.POWER_SERVICE) as PowerManager).run {
                newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "Itchtector::startTracking").apply {
                    acquire(C.TIMEOUT_AWAKE)
                }
            }
        val wm = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
        wifiLock = wm.createWifiLock(WifiManager.WIFI_MODE_FULL, "Itchtector::startTracking:wifi")
        wifiLock?.acquire()

        compositeDisposable.add(getStopwatchSubscriber())

        AppUtil.startService(this, Intent(applicationContext, BraceletManagerService::class.java)
            .putExtra(S.IS_START_BUTTON_PRESSED, true)
            .putExtra(S.SURVEY_ID, surveyId))

        AppUtil.startService(this, Intent(applicationContext, PhoneSensorService::class.java)
            .putExtra(S.IS_START_BUTTON_PRESSED, true)
            .putExtra(S.SURVEY_ID, surveyId))

        startRecording()
        compositeDisposable.add(getMicSubscriber())

        Realm.getDefaultInstance().use { realm ->
            val survey = realm.where(Survey::class.java)
                .equalTo("id", surveyId!!)
                .findFirst()

            realm.beginTransaction()
            survey.isTrackingComplete = true    // Set true beforehand in case of handling app crash
            survey.isSurveyClosed = false
            realm.commitTransaction()
        }

        binding!!.textBtnContinue.text = "측정 완료"
    }

    private fun stopTracking(isTrackingComplete: Boolean, isSurveyClosed: Boolean) {
        wifiLock?.release()
        wakeLock?.release()

        Realm.getDefaultInstance().use { realm ->
            val survey = realm.where(Survey::class.java)
                .equalTo("id", surveyId!!)
                .findFirst()

            realm.beginTransaction()
            survey.isTrackingComplete = isTrackingComplete
            survey.isSurveyClosed = isSurveyClosed
            realm.commitTransaction()
        }

        if (isSurveyClosed) {
            RxBus.getInstance()!!.send(UnsentCompleteSignal(surveyId!!))
        }

        compositeDisposable.clear()

        AppUtil.startService(this, Intent(applicationContext, BraceletManagerService::class.java)
            .putExtra(S.IS_STOP_BUTTON_PRESSED, true))

        AppUtil.startService(this, Intent(applicationContext, PhoneSensorService::class.java)
            .putExtra(S.IS_STOP_BUTTON_PRESSED, true))

        stopRecording()
    }

    private fun backToMain() {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right)
    }

    private fun moveToSurvey() {
        val intent = Intent(applicationContext, SurveyActivity::class.java)
        intent.putExtra(S.SURVEY_INDEX, RC.SURVEY_POST_1)
        intent.putExtra(S.SURVEY_ID, surveyId)
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    private var stopwatch: Long = 0 // millis
    private fun getStopwatchSubscriber(): Disposable{
        stopwatch = 0
        return RxBus.getInstance()!!.toObservable(RawData::class.java)
            .buffer(C.STOPWATCH_TIMESPAN, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { arr: List<RawData> ->
                if (!arr.isEmpty()) {
                    stopwatch += C.STOPWATCH_TIMESPAN
                    val timePassed = stopwatch / 1000
                    binding!!.stopwatch.text =
                        DateUtil.getTimeFormatFromSeconds(timePassed,
                            "HH : mm : ss",
                            TimeZone.getTimeZone("GMT"))
                }
            }
    }

    private fun onClickBraceletItem(position: Int) {
        BraceletKeeper.braceletMap[position]?.let { bracelet ->
            if (bracelet.status == DeviceStatus.CONNECTED) {
                bracelet.ledTick = 500
                bracelet.ledDuration = 4000
            }
        }
    }

    private fun prepareRecording() {
        bufferSize = AudioRecord.getMinBufferSize(sampleRate, channelCount, audioFormat)
    }

    private fun startRecording() {
        bufferSize = AudioRecord.getMinBufferSize(sampleRate, channelCount, audioFormat)

        recorder = AudioRecord(audioSource, sampleRate, channelCount, audioFormat, bufferSize)
        recorder?.startRecording()
        isRecording = true
    }

    private fun stopRecording() {
        recorder?.stop()
        recorder?.release()
        isRecording = false

        closeFileStateAndSend()
    }

    private fun getMicSubscriber(): Disposable {
        val bracket = "[M]"
        return Observable.create<ByteArray> { emitter ->
                val chunk = ByteArray(bufferSize)
                while (isRecording) {
                    val size = recorder?.read(chunk, 0, bufferSize)
                    if (size != null && size > 0) {
                        emitter.onNext(chunk)
                    }
                }
                emitter.onComplete()
            }
            .subscribeOn(Schedulers.io())
            .map { chunk ->
                prepareFileState()

                if (currentFileState == null) {
                    throw Throwable("currentFileState is null")
                }

                if (currentFileState?.isWritable()!!) {
                    currentFileState?.write(chunk)

                    "$bracket Write: ${chunk.size} byte(s)"
                } else {
                    throw Throwable("file is not writable")
                }
            }
            .subscribe({ }, {e -> handleError(e)}, {
                closeFileStateAndSend()
            })
    }

    private fun prepareFileState() {
        if (currentFileState != null && currentFileState?.size!! > C.MAX_FILE_SIZE) {
            currentFileState?.closeFileOutputStreams()
            sendFile(currentFileState?.id!!)
            currentFileState = null
        }

        if (currentFileState == null || !currentFileState?.isWritable()!!) {
            val userKey= SessionHelper.getUserKey()!!
            val dirPath = userKey + "/" + DateUtil.getStringDateFormat("yyyy/MM/dd")
            val filename = DateUtil.getStringDateFormat("yyyyMMdd_HHmmss_SSS") + "_MIC.pcm"

            val newFileState = FileState(DataKind.PHONE_MIC.toString(), C.INDEX_PHONE_MIC, surveyId!!, userKey, dirPath, filename)

            if (newFileState.makeDir()) {
                newFileState.openFileOutputStreams()
                currentFileState = newFileState
            } else {
                newFileState.closeFileOutputStreams()
            }

            try {
                Realm.getDefaultInstance().use { realm ->
                    realm.beginTransaction()
                    realm.copyToRealm(newFileState)
                    realm.commitTransaction()
                }
            } catch (e: Throwable) {
                handleError(e)
            }
        }
    }

    private fun closeFileStateAndSend() {
        if (currentFileState != null && currentFileState?.size!! > 0) {
            currentFileState!!.closeFileOutputStreams()
            sendFile(currentFileState!!.id)
            currentFileState = null
        }
    }

    private fun sendFile(fileStateId: String) {
        RxBus.getInstance()!!.send(NewFile(fileStateId))
    }

    private fun handleError(tag: String, e: Throwable) {
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}