package com.itchtector.itchtector_alpha_v2.constant;

object RC { // RequestCode
    val DEV = 123
    const val SIGN_IN = 10000

    const val RECORDER = 30000
    const val UPLOAD_VIEWER = 30100
    const val SURVEY_PRE_1 = 101
    const val SURVEY_PRE_2 = 102
    const val SURVEY_POST_1 = 201
    const val SURVEY_POST_2 = 202
}