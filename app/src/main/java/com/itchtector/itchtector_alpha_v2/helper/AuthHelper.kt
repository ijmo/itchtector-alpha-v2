package com.itchtector.itchtector_alpha_v2.helper

import com.crashlytics.android.Crashlytics
import com.firebase.ui.auth.AuthUI
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.iid.FirebaseInstanceId
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.reactivex.Maybe
import io.reactivex.functions.BiFunction
import io.reactivex.schedulers.Schedulers
import java.util.*

object AuthHelper {
    private val TAG = AuthHelper::class.java.simpleName

    val providers: MutableList<AuthUI.IdpConfig> = Arrays.asList(
        AuthUI.IdpConfig.EmailBuilder().build()
//        , AuthUI.IdpConfig.PhoneBuilder().build()
//        , AuthUI.IdpConfig.GoogleBuilder().build()
//        , AuthUI.IdpConfig.FacebookBuilder().build()
    )!!

    fun getTokenPair(): Pair<String?, String?> {
        val refreshToken = SessionHelper.getRefreshToken()
        val accessToken = SessionHelper.getAccessToken()

        return Pair(refreshToken, accessToken)
    }

    fun getFirebaseTokenMaybe(): Maybe<String> {
        return Maybe.create { emitter ->
            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener { instanceIdResult ->
                val token = instanceIdResult.token
                LogUtil.d(TAG, "Firebase token: $token")
                emitter.onSuccess(token)
            }.addOnFailureListener { e ->
                emitter.onError(e)
            }
        }
    }

    fun getFirebaseUserMaybe(): Maybe<Pair<String, String>> {
        return Maybe.create { emitter ->
            FirebaseAuth.getInstance().currentUser!!.getIdToken(true)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val result = task.result!!
                        val signInProvider = result.signInProvider!!
                        val token = result.token!!
                        LogUtil.d(TAG, "Firebase token: $token")
                        emitter.onSuccess(Pair(token, signInProvider))
                    } else {
                        handleError(Throwable("getFirebaseUserMaybe() error!!! ${task.exception?.message}"))
                        emitter.onError(task.exception!!)
                    }
                }
        }
    }

    fun getRefreshTokenAndUserKeyMaybe(): Maybe<Pair<String, String>> {
        return Maybe.create<Pair<String, String>> { emitter ->
            val refreshToken = SessionHelper.getRefreshToken()
            val userKey = SessionHelper.getUserKey()
            if (refreshToken != null && userKey != null) {
                emitter.onSuccess(Pair(refreshToken, userKey))
            } else {
                emitter.onError(Throwable(""))
            }
        }.onErrorResumeNext(
            Maybe
                .zip(ApiHelper.getInstanceIdMaybe()
                    , getFirebaseUserMaybe()
                    , BiFunction<String, Pair<String, String>, Triple<String, String, String>> {
                            instanceId, (firebaseToken, signInProvider) ->
                    Triple(instanceId, firebaseToken, signInProvider)
                }).flatMap { (instanceId, firebaseToken, signInProvider) ->
                    HttpApiHelper.getLoginMaybe(instanceId, firebaseToken, signInProvider)
                        .subscribeOn(Schedulers.io())
                }.doOnSuccess { (refreshToken, accessToken, userKey) ->
                    SessionHelper.putRefreshToken(refreshToken)
                    SessionHelper.putAccessToken(accessToken)
                    SessionHelper.putUserKey(userKey)
                }.map { (refreshToken, _, userKey) ->
                    Pair(refreshToken, userKey)
                }
            )
    }

    fun getAccessTokenMaybe(): Maybe<String> {
        return Maybe.create<String> { emitter ->
            val token = SessionHelper.getAccessToken()
            if (token != null) {
                emitter.onSuccess(token)
            } else {
                emitter.onError(Throwable(""))
            }
        }.onErrorResumeNext(HttpApiHelper.getNewAccessTokenMaybe())
    }

    private fun handleError(tag: String, e: Throwable) {
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}