package com.itchtector.itchtector_alpha_v2.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayList
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.itchtector.itchtector_alpha_v2.R
import com.itchtector.itchtector_alpha_v2.databinding.UploadListItemBinding
import com.itchtector.itchtector_alpha_v2.viewmodel.UploadState

class UploadAdapter
    : RecyclerView.Adapter<UploadAdapter.ViewHolder>() {

    var uploadList: ObservableArrayList<UploadState> = ObservableArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UploadAdapter.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.upload_list_item, parent, false
        )

        return ViewHolder(binding as UploadListItemBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = uploadList[position]
        viewHolder.bind(item!!)
    }

    override fun getItemCount(): Int {
        return uploadList.size
    }

    inner class ViewHolder(var binding: UploadListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: UploadState) {
            binding.item = item
        }
    }
}