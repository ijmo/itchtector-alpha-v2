package com.itchtector.itchtector_alpha_v2.viewmodel

import com.itchtector.itchtector_alpha_v2.constant.FileStatus

class FileUploadState(var status: FileStatus,
                      var progress: Int = 0)