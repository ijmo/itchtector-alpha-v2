package com.itchtector.itchtector_alpha_v2.bus.message

class RawData(var index: Int, val kind: String, val message: String)