package com.itchtector.itchtector_alpha_v2.helper

import com.crashlytics.android.Crashlytics
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers

object ApiHelper {
    private val TAG = ApiHelper::class.java.simpleName

    fun getInstanceIdMaybe(): Maybe<String> {
        return Maybe.create<String>{ emitter ->
            val instanceId = PreferenceHelper.getStringValueDefault(S.INSTANCE_ID)
            if (instanceId != null) {
                emitter.onSuccess(instanceId)
            } else {
                emitter.onError(Throwable(""))
            }
        }.onErrorResumeNext(HttpApiHelper
            .getRegisterInstanceMaybe(AppUtil.uuid())
            .subscribeOn(Schedulers.io()))
    }

    private fun handleError(tag: String, e: Throwable) {
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}