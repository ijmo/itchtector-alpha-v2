package com.itchtector.itchtector_alpha_v2.util

import android.content.ContentResolver
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Environment
import android.provider.Settings
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import java.util.*
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import com.itchtector.itchtector_alpha_v2.MyApplication
import com.itchtector.itchtector_alpha_v2.constant.NetworkStatus
import java.io.File
import android.content.Intent
import android.content.pm.PackageInfo
import android.net.Uri
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.InternetObservingSettings
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.strategy.SocketInternetObservingStrategy


object AppUtil {
    private val TAG = AppUtil::class.java.simpleName

    fun sleep(millis: Int) {
        try {
            Thread.sleep(millis.toLong())
        } catch (e: InterruptedException) {
            LogUtil.e(TAG, "${e.message}")
        }
    }

    val appRootPath:String
        get() = Environment.getExternalStorageDirectory().absolutePath + "/itchtector"

    val isExternalStorageWritable:Boolean
        get() {
            val state = Environment.getExternalStorageState()
            return Environment.MEDIA_MOUNTED == state
        }

    fun isInternetConnected(): Boolean {
        val cm = MyApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = cm.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnected
    }

    fun getConnectionType(): NetworkStatus {
        var result = NetworkStatus.DISCONNECTED
        val cm = MyApplication.getContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            cm?.run {
                cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                    if (hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                        result = NetworkStatus.WIFI
                    } else if (hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                        result = NetworkStatus.CELLULAR
                    }
                }
            }
        } else {
            cm?.run {
                cm.activeNetworkInfo?.run {
                    if (type == ConnectivityManager.TYPE_WIFI) {
                        result = NetworkStatus.WIFI
                    } else if (type == ConnectivityManager.TYPE_MOBILE) {
                        result = NetworkStatus.CELLULAR
                    }
                }
            }
        }
        return result
    }

    fun hasPermission(context: Context, permission: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            context.checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED
        } else {
            true
        }
    }

    fun getStringAfter(delimiter:String, str:String):String? {
        val arrString = str.split(delimiter.toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
        return if (arrString.size > 1) {
            arrString[1]
        } else null
    }

    fun getDeviceId(contentResolver:ContentResolver):String {
        return Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
    }

    fun uuid():String {
        return UUID.randomUUID().toString()
    }

    fun getColorWithAlpha(color:Int, ratio:Float):Int {
        var newColor = 0
        val alpha = Math.round(Color.alpha(color) * ratio)
        val r = Color.red(color)
        val g = Color.green(color)
        val b = Color.blue(color)
        newColor = Color.argb(alpha, r, g, b)
        return newColor
    }

    fun stringJoin(delimiter:CharSequence, objects:Array<Any>, endsWith:CharSequence):String {
        if (objects.isEmpty()) {
            return ""
        }
        val sb = StringBuilder()
        var i = 0
        while (i < objects.size - 1) {
            sb.append(objects[i])
            sb.append(delimiter)
            i++
        }
        sb.append(objects[i])
        sb.append(endsWith)

        return sb.toString()
    }

    fun hideKeyboard(activity: AppCompatActivity) {
        val imm = activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = activity.currentFocus
        if (view == null) {
            view = View(activity)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }

    fun deleteFile(context: Context, file: File): Boolean {
        if (file.delete()) {
            context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(File(file.toURI()))))
            return true

//            MediaScannerConnection.scanFile(context,
//                arrayOf(Environment.getExternalStorageDirectory().toString()), null) { path, uri ->
//                LogUtil.d(TAG, "${file.absolutePath} - DELETED")
//            }
        }
        return false
    }

    fun startService(context: Context, intent: Intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            context.startForegroundService(intent)
        } else {
            context.startService(intent)
        }
    }

    val internetObservingSetting = InternetObservingSettings.builder()
        .host("8.8.8.8")
        .port(53)
        .strategy(SocketInternetObservingStrategy())
        .build()

    fun getPackageInfo(context: Context): PackageInfo? {
        try {
            return context.packageManager.getPackageInfo(context.packageName, 0)
        } catch (e: PackageManager.NameNotFoundException) {
            e.printStackTrace()
        }

        return null
    }
}