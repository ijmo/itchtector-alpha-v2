package com.itchtector.itchtector_alpha_v2.constant

enum class NetworkStatus {
    DISCONNECTED, WIFI, CELLULAR
}