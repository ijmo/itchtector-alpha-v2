package com.itchtector.itchtector_alpha_v2.viewmodel

import androidx.databinding.ObservableField
import com.itchtector.itchtector_alpha_v2.constant.FileStatus
import com.itchtector.itchtector_alpha_v2.db.model.Survey
import com.itchtector.itchtector_alpha_v2.util.DateUtil
import io.realm.Realm

class UploadState(val surveyId: String) {
    private var map = HashMap<String, FileUploadState>()

    val subject: ObservableField<String> = ObservableField("")
    val statusRight: ObservableField<String> = ObservableField("")
    val statusBottom: ObservableField<String> = ObservableField("")
    val progress: ObservableField<Int> = ObservableField(0)

    init {
        Realm.getDefaultInstance().use { realm ->
            val survey = realm.where(Survey::class.java).equalTo("id", surveyId).findFirst()
            val startTime = DateUtil.getTimeFormatFromSeconds(survey.timeCreation, "M월 d일 hh:mm:ss 에 시작한 세션")
            this.subject.set(startTime)
        }
        statusRight.set("0%")
    }

    private fun calculateProgress() {
        val total = map.size * 100
        var current = 0
        map.entries.forEach { entry ->
            val fileUploadState = entry.value
            current += if (fileUploadState.status == FileStatus.COMPLETE) {
                100
            } else {
                (fileUploadState.progress * 0.9).toInt()
            }
        }

        val percentage = current * 100 / total

        statusRight.set("$percentage%")
        progress.set(percentage)
    }

    fun addOrUpdate(fileStateId: String, status: FileStatus, progress: Int? = null) {
        if (fileStateId !in map) {
            map[fileStateId] = FileUploadState(status, 0)
        } else {
            val fileUploadState = map[fileStateId]!!
            fileUploadState.status = status
            if (progress != null) {
                fileUploadState.progress = progress
            }
        }

        calculateProgress()
    }

    fun remove(fileStateId: String) {
        map.remove(fileStateId)
    }
}