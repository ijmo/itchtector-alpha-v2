package com.itchtector.itchtector_alpha_v2.constant

enum class FileStatus {
    PENDING_UPLOADING_FILE, UPLOADING_FILE, PENDING_SENDING_METADATA, SENDING_METADATA, COMPLETE
}