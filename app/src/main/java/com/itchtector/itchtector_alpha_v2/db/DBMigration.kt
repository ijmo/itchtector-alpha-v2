package com.itchtector.itchtector_alpha_v2.db

import io.realm.DynamicRealm
import io.realm.RealmMigration

class DBMigration : RealmMigration {

    override fun migrate(realm: DynamicRealm?, oldVersion: Long, newVersion: Long) {
        val schema = realm!!.schema
        var currentVer = oldVersion

        if (oldVersion == 0L) {
            schema.get("Survey")
                .addField("isSurveyClosed", Boolean::class.java)
                .addField("isCompleteSignalSent", Boolean::class.java)

            schema.get("Survey").transform { obj ->
                obj.setBoolean("isSurveyClosed", true)
                obj.setBoolean("isCompleteSignalSent", true)
            }

            currentVer++
        }
    }
}