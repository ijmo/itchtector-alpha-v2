package com.itchtector.itchtector_alpha_v2

import androidx.databinding.ObservableArrayMap
import bolts.Task
import com.itchtector.itchtector_alpha_v2.constant.DeviceStatus
import com.itchtector.itchtector_alpha_v2.db.model.Bracelet
import io.realm.Realm

object BraceletKeeper {
    const val NUM_DEVICES: Int = 2
    val braceletMap: ObservableArrayMap<Int, Bracelet> = ObservableArrayMap()
    private val TAG = BraceletKeeper::class.java.simpleName!!

    init {
        for (i in 0 until NUM_DEVICES) {
            BraceletKeeper.braceletMap[i] = Bracelet(i)
        }
    }

    fun setBracelet(targetIdx: Int, bracelet: Bracelet?): Bracelet {
        if (bracelet == null) {
            val newBracelet = Bracelet(targetIdx)
            braceletMap[targetIdx] = newBracelet
            return newBracelet
        }

        val anotherIdx: Int = if (targetIdx == 0) {
            1
        } else {
            0
        }

        val targerBracelet = braceletMap[targetIdx]
        val anotherBracelet = braceletMap[anotherIdx]

        if (anotherBracelet?.metaWearBoard != null
            && anotherBracelet.metaWearBoard?.isConnected!!
            && anotherBracelet.macAddress == bracelet.macAddress) {
            anotherBracelet.disposeAll()
            anotherBracelet.metaWearBoard?.disconnectAsync()?.continueWith { task: Task<Void> ->
                braceletMap.remove(anotherIdx)
                braceletMap[anotherIdx] = Bracelet(anotherIdx)
                null
            }
        }

        if (targerBracelet?.metaWearBoard != null
            && targerBracelet.metaWearBoard?.isConnected!!) {
            targerBracelet.disposeAll()
            targerBracelet.metaWearBoard?.disconnectAsync()?.continueWith { task: Task<Void> ->
                braceletMap[targetIdx] = bracelet
                null
            }
        } else {
            braceletMap[targetIdx] = bracelet
        }

        return bracelet
    }

    fun getBothMacAddress(): Pair<String?, String?> {
        if (braceletMap[0]?.macAddress != null
                && braceletMap[1]?.macAddress != null) {
            return Pair(braceletMap[0]?.macAddress!!, braceletMap[0]?.macAddress!!)
        }
        return Pair(null, null)
    }

    fun checkBothStatus(status: DeviceStatus): Boolean {
        if (braceletMap[0]?.macAddress != null
            && braceletMap[1]?.macAddress != null
            && braceletMap[0]?.status == status
            && braceletMap[1]?.status == status) {
            return true
        }
        return false
    }

    fun removeBothFromRealm() {
        Realm.getDefaultInstance().use { realm ->
            val query =
                realm.where(Bracelet::class.java)
            val result = query.findAll()
            realm.beginTransaction()
            for (bracelet in result) {
                bracelet.deleteFromRealm()
            }
            realm.commitTransaction()
        }
    }
}