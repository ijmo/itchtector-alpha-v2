package com.itchtector.itchtector_alpha_v2.db.model

import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.DateUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.realm.RealmObject
import io.realm.annotations.Ignore
import io.realm.annotations.PrimaryKey
import java.io.*
import java.util.*

open class FileState(): RealmObject() {
    @PrimaryKey
    var id = UUID.randomUUID().toString()
    var kind: String? = null
    var subIndex: Int = -1
    var surveyId: String? = null
    var userKey: String? = null // Hashed value for user. app should have got this from server.
    var filename: String? = null
    var dirPath: String? = null
    var timeCreation = DateUtil.getTimeNow()
    var timeClosure = DateUtil.getTimeNow()
    var isFileCompressed: Boolean = false
    var isFileSent: Boolean = false
    var isMetadataSent: Boolean = false

    @Ignore private var TAG: String = ""
    @Ignore var size: Long = 0
    @Ignore var file: File? = null
    @Ignore var fos: FileOutputStream? = null
    @Ignore var dos: DataOutputStream? = null

    constructor(kind: String, subIndex: Int, surveyId: String, userKey: String, dirPath: String, filename: String) : this() {
        this.TAG = filename
        this.kind = kind
        this.subIndex = subIndex
        this.surveyId = surveyId
        this.userKey = userKey
        this.dirPath = dirPath
        this.filename = filename
    }

    private fun getAbsoluteDirPath(): String {
        return AppUtil.appRootPath + "/" + dirPath
    }

    fun getFilepath(): String {
        return "$dirPath/$filename"
    }

    fun getAbsoluteFilepath(): String {
        return getAbsoluteDirPath() + "/" + filename
    }

    fun write(b: ByteArray) {
        try {
            dos?.write(b)
            size += b.size
        } catch (e: Throwable) {
            LogUtil.e(TAG, e.message!!)
        }

    }

    fun makeDir(): Boolean {
        if (AppUtil.isExternalStorageWritable) {
            val file = File(getAbsoluteDirPath())
            if (!file.exists()) {
                file.mkdirs()
            }
            return true
        }

        return false
    }

    fun isWritable(): Boolean {
        return !(dos == null || fos == null)
    }

    fun openFileOutputStreams() {
        file = File(getAbsoluteFilepath())
        try {
            fos = FileOutputStream(file)
            dos = DataOutputStream(BufferedOutputStream(fos))
        } catch (e: Throwable) {
            closeFileOutputStreams()
            LogUtil.e(TAG, e.message!!)
        }
    }

    fun closeFileOutputStreams() {
        try {
            dos?.close()
            fos?.close()
            dos = null
            fos = null
        } catch (e: Throwable) {
            LogUtil.e(TAG, e.message!!)
        }

    }
}