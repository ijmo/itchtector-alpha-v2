package com.itchtector.itchtector_alpha_v2.db.model

import com.itchtector.itchtector_alpha_v2.util.DateUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey
import java.util.*

open class Survey : RealmObject() {
    @PrimaryKey
    var id = UUID.randomUUID().toString()
    var entries: RealmList<SurveyEntry> = RealmList()
    var isSent = false
    var isTrackingComplete = false
    var isSurveyClosed = true
    var isCompleteSignalSent = false
    var timeCreation = DateUtil.getTimeNow()
    var timeClosure = DateUtil.getTimeNow()

    fun addEntry(newEntry: SurveyEntry) {
        entries.reversed().filter { entry ->
            entry.key == newEntry.key
        }.forEach { entry ->
            entry.deleteFromRealm()
        }

        entries.add(newEntry)
    }
}