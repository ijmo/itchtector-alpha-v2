package com.itchtector.itchtector_alpha_v2.api

import io.reactivex.Maybe
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.POST

interface ItchtectorServiceApi {
    @POST("api/v1/auth/register-instance") @JvmSuppressWildcards
    fun registerInstance(@Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @POST("api/v1/auth/ping") @JvmSuppressWildcards
    fun ping(@Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @POST("api/v1/auth/firebase-login") @JvmSuppressWildcards
    fun login(@Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @POST("api/v1/auth/new-access-token") @JvmSuppressWildcards
    fun newAccessToken(@Header("Authorization") token: String): Maybe<Map<String, Any>>

    @POST("api/v1/notification/register-token") @JvmSuppressWildcards
    fun registerToken(@Header("Authorization") token: String, @Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @POST("api/v1/reception/survey-answer") @JvmSuppressWildcards
    fun sendSurveyAnswer(@Header("Authorization") token: String, @Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @POST("api/v1/reception/file-metadata") @JvmSuppressWildcards
    fun sendFileMetadata(@Header("Authorization") token: String, @Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @POST("api/v1/reception/file-tx-complete") @JvmSuppressWildcards
    fun sendFileTxComplete(@Header("Authorization") token: String, @Body body: Map<String, Any>): Maybe<Map<String, Any>>

    @GET("api/v1/results/") @JvmSuppressWildcards
    fun getResult(@Header("Authorization") token: String, @Body body: Map<String, Any>): Maybe<Map<String, Any>>
}