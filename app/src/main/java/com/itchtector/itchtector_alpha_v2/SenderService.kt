package com.itchtector.itchtector_alpha_v2

import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import androidx.databinding.ObservableArrayList
import com.amazonaws.mobile.client.AWSMobileClient
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.ObjectMetadata
import com.amazonaws.services.s3.model.StorageClass
import com.crashlytics.android.Crashlytics
import com.itchtector.itchtector_alpha_v2.bus.RxBus
import com.itchtector.itchtector_alpha_v2.constant.C
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.helper.ApiHelper
import com.itchtector.itchtector_alpha_v2.helper.HttpApiHelper
import com.itchtector.itchtector_alpha_v2.db.model.FileState
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.io.File
import java.util.concurrent.TimeUnit
import io.reactivex.android.schedulers.AndroidSchedulers
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.InternetObservingSettings
import com.github.pwittchen.reactivenetwork.library.rx2.internet.observing.strategy.SocketInternetObservingStrategy
import com.itchtector.itchtector_alpha_v2.bus.message.*
import com.itchtector.itchtector_alpha_v2.constant.FileStatus
import com.itchtector.itchtector_alpha_v2.constant.RC
import com.itchtector.itchtector_alpha_v2.db.model.Survey
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.viewmodel.UploadState
import io.reactivex.Maybe
import java.io.FileInputStream
import java.io.FileOutputStream
import java.util.zip.GZIPOutputStream


class SenderService : Service() {

    companion object {
        val pendingObjects = ObservableArrayList<UploadState>()
    }

    private val TAG = SenderService::class.java.simpleName
    private val compositeDisposable = CompositeDisposable()
    private var transferUtility: TransferUtility? = null
    private var isInitialized = false

    @SuppressLint("CheckResult")
    override fun onCreate() {
        super.onCreate()
        ApiHelper.getInstanceIdMaybe().subscribe({ }, { e ->
            handleError(e)
        })
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            when {
                intent.getBooleanExtra(S.IS_AWS_CLIENT_INITIALZIED, false) -> {
                    if (!isInitialized) {
                        initService()
                    }
                }
            }
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

            val channelId = "itchtector.senderService"
            val chan = NotificationChannel(channelId,
                "이치텍터 데이터 전송 서비스", NotificationManager.IMPORTANCE_NONE)
            chan.lightColor = Color.BLUE
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            service.createNotificationChannel(chan)

            val builder = Notification.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true)

            val notification = builder.build()
            startForeground(1, notification)
        } else {
            val builder = NotificationCompat.Builder(this)
                .setContentTitle(getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)

            val notification = builder.build()

            startForeground(1, notification)
        }
        return START_STICKY
    }

    private fun initService() {
        isInitialized = true
        transferUtility = TransferUtility.builder()
            .context(applicationContext)
            .awsConfiguration(AWSMobileClient.getInstance().configuration)
            .s3Client(AmazonS3Client(AWSMobileClient.getInstance().credentialsProvider))
            .build()

        compositeDisposable.clear()
        compositeDisposable.add(getPingSenderSubscriber())
        compositeDisposable.add(getInternetConnectionSubscriber())
        compositeDisposable.add(getSurveySenderSubscriber())
        compositeDisposable.add(getFileCompressorSubscriber())
        compositeDisposable.add(getFileSenderSubscriber())
        compositeDisposable.add(getMetadataSenderSubscriber())
        compositeDisposable.add(getCompleteSignalSenderSubscriber())

        doUnfinishedTasks()
    }

    override fun onBind(intent: Intent?): IBinder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.clear()
    }

    private fun doUnfinishedTasks() {
        Realm.getDefaultInstance().use { realm ->
            val fileStates = realm.where(FileState::class.java)
                .equalTo("isFileCompressed", false).or()
                .equalTo("isFileSent", false).or()
                .equalTo("isMetadataSent", false)
                .findAll()

            fileStates.forEach { fileState ->

                val surveyId = fileState.surveyId!!
                val filename = fileState.filename!!

                if (!fileState.isFileCompressed) {
                    RxBus.getInstance()!!.send(NewFile(fileState.id))
                } else if (!fileState.isFileSent) {
                    updatePendingStatus(surveyId, fileState.id, FileStatus.PENDING_UPLOADING_FILE)
                    RxBus.getInstance()!!.send(UnsentFile(fileState.id))
                } else if (!fileState.isMetadataSent) {
                    updatePendingStatus(surveyId, fileState.id, FileStatus.PENDING_SENDING_METADATA)
                    RxBus.getInstance()!!.send(UnsentMetadata(fileState.id))
                }
            }

            val surveys = realm.where(Survey::class.java)
                .equalTo("isSent", false)
                .equalTo("isTrackingComplete", true)
                .findAll()

            surveys.forEach { survey ->
                RxBus.getInstance()!!.send(UnsentSurvey(survey.id))
            }


            val uncompleted = realm.where(Survey::class.java)
                .equalTo("isTrackingComplete", true)
                .equalTo("isCompleteSignalSent", false)
                .findAll()

            uncompleted.forEach { survey ->
                RxBus.getInstance()!!.send(UnsentCompleteSignal(survey.id))
            }
        }
    }

    private fun getPingSenderSubscriber(): Disposable {
        return Observable
            .interval(C.INTERVAL_LOOPER_PING, TimeUnit.MILLISECONDS)
            .doOnNext {
                LogUtil.v(TAG, "Trying to send a ping...")
            }.flatMap {
                ApiHelper.getInstanceIdMaybe().toObservable()
            }.subscribeOn(Schedulers.io())
            .flatMap { instanceId ->
                HttpApiHelper.getPingMaybe(instanceId).toObservable()
            }.onErrorResumeNext(Observable.empty())
            .subscribe({ result ->
                LogUtil.v(TAG, result)
            }, { e ->
                handleError(e)
            })
    }

    private fun getInternetConnectionSubscriber(): Disposable {
        val settings = InternetObservingSettings.builder()
//            .interval(C.INTERVAL_CHECK_INTERNET)
            .host("8.8.8.8")
            .port(53)
            .strategy(SocketInternetObservingStrategy())
            .build()

        return ReactiveNetwork
            .observeInternetConnectivity(settings)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnNext { isConnectedToInternet ->
                LogUtil.d(TAG, "isConnectedToInternet=$isConnectedToInternet ${AppUtil.getConnectionType()}")
                if (isConnectedToInternet) {

                }
            }.subscribe()
    }

    private fun getSurveyById(surveyId: String): Survey? {
        var survey: Survey? = null
        Realm.getDefaultInstance().use { realm ->
            survey = realm.where(Survey::class.java).equalTo("id", surveyId).findFirst()
            survey = realm.copyFromRealm(survey)
        }
        return survey
    }

    private fun getFileStateById(fileStateId: String): FileState? {
        var fileState: FileState? = null
        Realm.getDefaultInstance().use { realm ->
            fileState = realm.where(FileState::class.java).equalTo("id", fileStateId).findFirst()
            fileState = realm.copyFromRealm(fileState)
        }
        return fileState
    }

    private fun updatePendingStatus(surveyId: String, fileStateId: String, status: FileStatus, progress: Int? = null) {
        if (pendingObjects.none { uploadState -> uploadState.surveyId == surveyId }) {
            pendingObjects.add(0, UploadState(surveyId))
        }

        pendingObjects.filter { uploadState -> uploadState.surveyId == surveyId }
            .forEach { uploadState -> uploadState.addOrUpdate(fileStateId, status, progress)}
    }

    private fun getSurveySenderSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(UnsentSurvey::class.java)
            .subscribeOn(Schedulers.io())
            .concatMapMaybe { unsentSurvey ->
                val survey = getSurveyById(unsentSurvey.surveyId)!!
                HttpApiHelper.getSurveyPostMaybe(survey)
                    .subscribeOn(Schedulers.io())
            }.doOnError{ e -> handleError("$TAG:getSurveySenderSubscriber()", e) }
            .onErrorResumeNext(Observable.empty())
            .subscribe{ surveyId ->
                Realm.getDefaultInstance().use { realm ->
                    val survey = realm.where(Survey::class.java)
                        .equalTo("id", surveyId)
                        .findFirst()

                    realm.beginTransaction()
                    survey.isSent = true
                    realm.commitTransaction()

                    if (!survey.entries.none { surveyEntry ->
                            surveyEntry.key == RC.SURVEY_POST_2.toString() }) {
                        LogUtil.d(TAG, "$surveyId - COMPLETE Sending Survey")
                    }
                }
            }
    }

    private fun getFileCompressorSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(NewFile::class.java)
            .subscribeOn(Schedulers.io())
            .concatMap { newFile ->
                val fileStateId = newFile.fileStateId
                val fileState: FileState? = getFileStateById(fileStateId)

                when {
                    fileState == null -> throw Throwable("No file: $fileStateId")
                    fileState.isFileCompressed -> Observable.empty()
                    else -> {
                        val surveyId = fileState?.surveyId!!
                        val file = File(fileState.getAbsoluteFilepath())
                        val gzipFilepath = "${fileState.getAbsoluteFilepath()}.gz"

                        val buffer = ByteArray(4096)
                        var len = 0

                        FileInputStream(file).use { fis ->
                            var sizeFileRead = 0
                            GZIPOutputStream(FileOutputStream(gzipFilepath)).use { gzos ->
                                while ({ len = fis.read(buffer); len }() > 0) {
                                    sizeFileRead += len
                                    gzos.write(buffer, 0, len)
                                }
                            }
                        }

                        // Delete original file
                        AppUtil.deleteFile(this, file)

                        Realm.getDefaultInstance().use { realm ->
                            val fs = realm.where(FileState::class.java)
                                .equalTo("id", fileStateId).findFirst()
                            realm.beginTransaction()
                            fs.filename = "${fs.filename}.gz"
                            fs.isFileCompressed = true
                            realm.commitTransaction()
                        }

                        updatePendingStatus(surveyId, fileStateId, FileStatus.PENDING_UPLOADING_FILE, 0)

                        Observable.just(fileStateId)
                    }
                }
            }.doOnError{ e -> handleError("$TAG:getFileCompressorSubscriber()", e) }
            .onErrorResumeNext(Observable.empty())
            .subscribe { fileStateId ->
                RxBus.getInstance()!!.send(UnsentFile(fileStateId))
            }
    }

    private fun getFileSenderSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(UnsentFile::class.java)
            .subscribeOn(Schedulers.io())
            .concatMapMaybe { unsentFile ->
                val fileStateId = unsentFile.fileStateId
                val fileState: FileState? = getFileStateById(fileStateId)

                if (fileState == null) {
                    throw Throwable("No file: $fileStateId")
                } else {
                    val surveyId = fileState.surveyId!!
                    val file = File(fileState.getAbsoluteFilepath())
                    val filename = fileState.filename!!
                    val filepath = fileState.getFilepath()

                    val metadata = ObjectMetadata()
                    metadata.contentType = "application/gzip"
                    metadata.setStorageClass(StorageClass.StandardInfrequentAccess)

                    val uploadObserver = transferUtility!!.upload(
                        S.S3_BUCKET_SENSOR_DATA,
                        "uploads/$filepath",
                        file,
                        metadata
                    )

                    // Start to upload
                    Maybe.create<String> { emitter ->
                        uploadObserver?.setTransferListener(object : TransferListener {
                            override fun onStateChanged(id: Int, state: TransferState) {
                                if (TransferState.COMPLETED == state) {
                                    LogUtil.d(TAG, "$filename - COMPLETE Sending File")
                                    // Set file sent flag to true
                                    Realm.getDefaultInstance().use { realm ->
                                        val fs =
                                            realm.where(FileState::class.java)
                                                .equalTo("id", fileStateId).findFirst()
                                        realm.beginTransaction()
                                        fs.isFileSent = true
                                        realm.commitTransaction()
                                    }
                                    updatePendingStatus(surveyId, fileStateId, FileStatus.PENDING_SENDING_METADATA)

                                    emitter.onSuccess(fileStateId)
                                }
                            }

                            override fun onProgressChanged(id: Int, bytesCurrent: Long, bytesTotal: Long) {
                                val percentDonef = bytesCurrent.toFloat() / bytesTotal.toFloat() * 100
                                val percentDone = percentDonef.toInt()
                                val progress = "bytesCurrent: $bytesCurrent of $bytesTotal $percentDone%"
                                LogUtil.v(TAG, "$filename: ID=$id $progress")

                                updatePendingStatus(surveyId, fileStateId, FileStatus.UPLOADING_FILE, percentDone)
                            }

                            override fun onError(id: Int, e: Exception) {
//                                emitter.onError(e)
                                handleError(e)
                            }
                        })
                    }
                }
            }.doOnError{ e -> handleError("$TAG:getFileSenderSubscriber()", e) }
            .onErrorResumeNext(Observable.empty())
            .subscribeOn(Schedulers.io())
            .subscribe { fileStateId ->
                RxBus.getInstance()!!.send(UnsentMetadata(fileStateId))
            }
    }

    private fun getMetadataSenderSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(UnsentMetadata::class.java)
            .subscribeOn(Schedulers.io())
            .concatMapMaybe { unsentMetadata ->
                val fileStateId = unsentMetadata.fileStateId
                val fileState: FileState? = getFileStateById(fileStateId)

                if (fileState == null) {
                    throw Throwable("No file: $fileStateId")
                } else {
                    val surveyId = fileState.surveyId!!
                    val userKey = fileState.userKey!!
                    val dirPath = fileState.dirPath!!
                    val filename = fileState.filename!!
                    val kind = fileState.kind

                    updatePendingStatus(surveyId, fileStateId, FileStatus.SENDING_METADATA)

                    HttpApiHelper
                        .getMetadataPostMaybe(fileStateId, surveyId, userKey, dirPath, filename, kind)
                        .subscribeOn(Schedulers.io())
                }
            }.doOnError { e -> handleError("$TAG:getMetadataSenderSubscriber()", e) }
            .onErrorResumeNext(Observable.empty())
            .subscribe { fileStateId ->
                // Delete from Realm
                var filename: String? = null
                Realm.getDefaultInstance().use { realm ->
                    val fs = realm.where(FileState::class.java).equalTo("id", fileStateId).findFirst()
                    val surveyId = fs.surveyId!!
                    filename = fs.filename
                    val file = File(fs.getAbsoluteFilepath())
                    realm.beginTransaction()
                    fs.isMetadataSent = true
                    realm.commitTransaction()
                    // Delete the file from external storage
                    if (file.exists()) {
                        AppUtil.deleteFile(this, file)
                    }

                    updatePendingStatus(surveyId, fileStateId, FileStatus.COMPLETE)

                    RxBus.getInstance()!!.send(UnsentCompleteSignal(surveyId))
                }
                LogUtil.d(TAG, "$filename - COMPLETE Sending Metadata")
            }
    }

    private fun getCompleteSignalSenderSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(UnsentCompleteSignal::class.java)
            .subscribeOn(Schedulers.io())
            .filter { unsentCompleteSignal ->
                val surveyId = unsentCompleteSignal.surveyId
                Realm.getDefaultInstance().use { realm ->
                    val survey = realm.where(Survey::class.java)
                        .equalTo("id", surveyId)
                        .equalTo("isTrackingComplete", true)
                        .equalTo("isCompleteSignalSent", false)
                        .findFirst()

                    val fileStates = realm.where(FileState::class.java)
                        .equalTo("surveyId", surveyId)
                        .equalTo("isMetadataSent", false)
                        .findAll()

                    survey != null
                            && fileStates.size < 1
                }
            }.concatMapMaybe { unsentCompleteSignal ->
                HttpApiHelper.getFileTxCompletePostMaybe(unsentCompleteSignal.surveyId)
                    .subscribeOn(Schedulers.io())
            }.doOnError { e -> handleError("$TAG:getCompleteSignalSenderSubscriber()", e) }
            .onErrorResumeNext(Observable.empty())
            .subscribe{ surveyId ->
                Realm.getDefaultInstance().use { realm ->
                    val survey = realm.where(Survey::class.java)
                        .equalTo("id", surveyId)
                        .findFirst()
                    realm.beginTransaction()
                    survey.isCompleteSignalSent = true
                    realm.commitTransaction()
                }
                LogUtil.d(TAG, "$surveyId - COMPLETE Sending complete signal")
            }
    }

    private fun handleError(tag: String, e: Throwable) {
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}