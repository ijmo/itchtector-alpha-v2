package com.itchtector.itchtector_alpha_v2.viewmodel

class ChecklistItem(var description: String, var abbreviation: String, var group: Int, var isEditable: Boolean, var checked: Boolean, var hint: String? = "")