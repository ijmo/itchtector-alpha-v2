package com.itchtector.itchtector_alpha_v2

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.itchtector.itchtector_alpha_v2.adapter.UploadAdapter
import com.itchtector.itchtector_alpha_v2.databinding.UploadActivityBinding

class UploadActivity : AppCompatActivity() {

    private val TAG: String = UploadActivity::class.simpleName!!
    private var binding: UploadActivityBinding? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.upload_activity)
        binding!!.uploadList = SenderService.pendingObjects

        binding!!.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding!!.recyclerView.adapter = UploadAdapter()

        val toolbar = binding!!.toolbar
        setSupportActionBar(toolbar)
        supportActionBar?.title = "데이터 저장"
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        toolbar.setNavigationIcon(R.mipmap.icon_close)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        overridePendingTransition(android.R.anim.fade_in, R.anim.slide_out_bottom)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }
}