package com.itchtector.itchtector_alpha_v2.util

import java.text.SimpleDateFormat
import java.util.*

object DateUtil {
    fun getTimeNow(): Date {
        return Calendar.getInstance().time
    }

    fun getStringDateFormat(pattern: String): String {
        val sdf = SimpleDateFormat(pattern, Locale.US)
        return sdf.format(getTimeNow())
    }

    fun getTimeFormatFromSeconds(seconds: Long, pattern: String, timeZone: TimeZone = TimeZone.getDefault()): String {
        val date = Date(seconds * 1000)
        val sdf = SimpleDateFormat(pattern, Locale.getDefault())
        sdf.timeZone = timeZone
        return sdf.format(date)
    }

    fun getTimeFormatFromSeconds(date: Date, pattern: String, timeZone: TimeZone = TimeZone.getDefault()): String {
        val sdf = SimpleDateFormat(pattern, Locale.getDefault())
        sdf.timeZone = timeZone
        return sdf.format(date)
    }
}