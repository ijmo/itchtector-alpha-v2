package com.itchtector.itchtector_alpha_v2.util

import android.media.MediaPlayer
import android.util.Log
import com.itchtector.itchtector_alpha_v2.MyApplication
import com.itchtector.itchtector_alpha_v2.R
import com.mbientlab.bletoolbox.scanner.BuildConfig


object LogUtil {
    private const val prefix = "@@"
    private var mediaPlayer: MediaPlayer? = null

    fun v(tag: String, msg: String) {
        v(tag, msg, prefix)
    }

    fun v(tag: String, msg: String, startsWith: String) {
        Log.v(tag, "$startsWith $msg")
    }

    fun d(tag: String, msg: String) {
        d(tag, msg, prefix)
    }

    fun d(tag: String, msg: String, startsWith: String) {
        Log.d(tag, "$startsWith $msg")
    }

    fun i(tag: String, msg: String) {
        i(tag, msg, prefix)
    }

    fun i(tag: String, msg: String, startsWith: String) {
        Log.i(tag, "$startsWith $msg")
    }

    fun w(tag: String, msg: String) {
        w(tag, msg, prefix)
    }

    fun w(tag: String, msg: String, startsWith: String) {
        Log.w(tag, "$startsWith $msg")
    }

    fun e(tag: String, e: Throwable) {
        e(tag, "${e::class.java.canonicalName!!}: ${e.message ?: ""}")
    }

    fun e(tag: String, msg: String) {
        e(tag, msg, prefix)

        if (BuildConfig.DEBUG) {
            if (mediaPlayer == null) {
                mediaPlayer = MediaPlayer.create(MyApplication.getContext(), R.raw.store_door_chime)
            }

            if (!mediaPlayer?.isPlaying!!) {
                mediaPlayer?.start()
            }
        }
        //        mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
        //            @Override
        //            public void onPrepared(MediaPlayer mp) {
        //                if (mp == mediaPlayer) {
        //                    mp.start();
        //                }
        //            }
        //        });
        //        mediaPlayer?.setOnCompletionListener { mp -> mp.release() }
    }

    fun e(tag: String, msg: String, startsWith: String) {
        Log.e(tag, "$startsWith $msg")
    }
}
