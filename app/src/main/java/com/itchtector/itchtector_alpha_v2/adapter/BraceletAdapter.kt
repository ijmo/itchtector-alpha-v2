package com.itchtector.itchtector_alpha_v2.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableArrayMap
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.itchtector.itchtector_alpha_v2.db.model.Bracelet
import com.itchtector.itchtector_alpha_v2.R
import com.itchtector.itchtector_alpha_v2.databinding.BraceletListItemBinding

class BraceletAdapter
    : RecyclerView.Adapter<BraceletAdapter.ViewHolder>() {

    var braceletMap: ObservableArrayMap<Int, Bracelet> = ObservableArrayMap()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var itemListener: (Int) -> Unit = { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BraceletAdapter.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.bracelet_list_item, parent, false
        )

        return ViewHolder(binding as BraceletListItemBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = braceletMap[position]
        viewHolder.bind(position, item!!, itemListener)  // FIXME: item is not exists
    }

    override fun getItemCount(): Int {
        return braceletMap.size
    }

    inner class ViewHolder(var binding: BraceletListItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int, item: Bracelet, itemListener: (Int) -> Unit) {
            binding.item = item
            binding.root.setOnClickListener { itemListener(position) }
        }
    }
}