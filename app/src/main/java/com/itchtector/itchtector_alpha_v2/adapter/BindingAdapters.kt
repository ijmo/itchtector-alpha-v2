package com.itchtector.itchtector_alpha_v2.adapter

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import android.widget.NumberPicker
import androidx.databinding.BindingAdapter
import androidx.databinding.ObservableArrayList
import androidx.databinding.ObservableArrayMap
import androidx.recyclerview.widget.RecyclerView
import com.itchtector.itchtector_alpha_v2.viewmodel.ChecklistItem
import com.itchtector.itchtector_alpha_v2.db.model.Bracelet
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import com.itchtector.itchtector_alpha_v2.viewmodel.UploadState
import com.jakewharton.rxbinding3.view.clicks
import java.util.concurrent.TimeUnit

@BindingAdapter("android:src")
fun setImageViewResource(imageView: ImageView, resource: Int) {
    imageView.setImageResource(resource)
}


@BindingAdapter("android:minValue")
fun setNumberPickerMinValue(numberPicker: NumberPicker, value: Int) {
    numberPicker.minValue = value
}

@BindingAdapter("android:maxValue")
fun setNumberPickerMaxValue(numberPicker: NumberPicker, value: Int) {
    numberPicker.maxValue = value
}

@BindingAdapter("bind:items")
fun setBindItems(recyclerView: RecyclerView, list: ArrayList<ChecklistItem>) {
    val adapter = recyclerView.adapter as ChecklistAdapter
    adapter.checklist = list
}

@BindingAdapter("bind:items")
fun setBindBraceletItems(recyclerView: RecyclerView, map: ObservableArrayMap<Int, Bracelet>) {
    val adapter = recyclerView.adapter as BraceletAdapter
    adapter.braceletMap = map
}

@BindingAdapter("bind:items")
fun setBindUploadItems(recyclerView: RecyclerView, list: ObservableArrayList<UploadState>) {
    val adapter = recyclerView.adapter as UploadAdapter
    adapter.uploadList = list
}

@SuppressLint("CheckResult")
@BindingAdapter("android:onClick")
fun setClickListener(view: View, listener: View.OnClickListener) {
    view.clicks().throttleFirst(3000L, TimeUnit.MILLISECONDS)
        .subscribe({
            listener.onClick(view)
        }, {e ->
            LogUtil.e("", e.message!!)
        })
}