package com.itchtector.itchtector_alpha_v2

import android.app.Dialog
import android.bluetooth.BluetoothDevice
import android.content.Intent
import android.content.pm.PackageManager
import android.content.res.Configuration
import android.os.Build
import android.os.Bundle
import androidx.core.view.GravityCompat
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import android.webkit.WebViewClient
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.res.ResourcesCompat
import androidx.databinding.DataBindingUtil
import androidx.drawerlayout.widget.DrawerLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.afollestad.materialdialogs.MaterialDialog
import com.amazonaws.mobile.client.AWSMobileClient
import com.crashlytics.android.Crashlytics
import com.firebase.ui.auth.AuthUI
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.itchtector.itchtector_alpha_v2.adapter.BraceletAdapter
import com.itchtector.itchtector_alpha_v2.bus.RxBus
import com.itchtector.itchtector_alpha_v2.constant.C
import com.itchtector.itchtector_alpha_v2.constant.DeviceStatus
import com.itchtector.itchtector_alpha_v2.constant.RC
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.databinding.MainActivityBinding
import com.itchtector.itchtector_alpha_v2.db.model.Survey
import com.itchtector.itchtector_alpha_v2.helper.AuthHelper
import com.itchtector.itchtector_alpha_v2.helper.SessionHelper
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import com.jakewharton.rxbinding3.view.clicks
import com.mbientlab.bletoolbox.scanner.BleScannerFragment
import com.mbientlab.metawear.MetaWearBoard
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import io.realm.RealmConfiguration
import retrofit2.HttpException
import java.util.UUID
import java.util.concurrent.TimeUnit

class MainActivity : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, BleScannerFragment.ScannerCommunicationBus {

    companion object {
        private val SERVICE_UUIDS = arrayOf(MetaWearBoard.METAWEAR_GATT_SERVICE)
    }

    private val TAG: String = MainActivity::class.simpleName!!
    private val compositeDisposable = CompositeDisposable()
    private var binding: MainActivityBinding? = null
    private var braceletAdapter: BraceletAdapter? = null
    private var drawerToggle: ActionBarDrawerToggle? = null
    private var dialog: Dialog? = null
    private var index: Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        LogUtil.d(TAG, "onCreate() - savedInstanceState=$savedInstanceState")
        binding = DataBindingUtil.setContentView(this, R.layout.main_activity)
        braceletAdapter = BraceletAdapter()
        braceletAdapter!!.itemListener = { i: Int -> onClickBraceletItem(i) }

        binding!!.activity = this
        binding!!.textVersion.text = "v${AppUtil.getPackageInfo(this)?.versionName}"
        binding!!.braceletMap = BraceletKeeper.braceletMap
        binding!!.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding!!.recyclerView.adapter = braceletAdapter

        val ws = binding!!.webview.settings
        ws.javaScriptEnabled = true
        ws.allowFileAccess = true
        ws.domStorageEnabled = true
        ws.allowContentAccess = true
        ws.allowFileAccessFromFileURLs = true
        ws.allowUniversalAccessFromFileURLs = true
        ws.setSupportZoom(true)
        binding!!.webview.webViewClient = WebViewClient()

        binding!!.btnDevStart.setOnClickListener { view ->
            Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                .setAction("Action", null).show()
            Crashlytics.getInstance().crash()
        }
        binding!!.btnDevStart.hide()

        setUpDialog()
        setUpActionBar()
        disableContinueButton()

        if (isPermissionGranted(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            initAWSMobileClient()
        }

        isPermissionGranted(android.Manifest.permission.ACCESS_COARSE_LOCATION)
        isPermissionGranted(android.Manifest.permission.RECORD_AUDIO)

//        SessionHelper.removeRefreshToken()
//        SessionHelper.removeUserKey()

        compositeDisposable.add(getDeviceStatusSubscriber())
        compositeDisposable.add(getSubscriberForWebView())

        checkUnfinishedSession()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId
        if (id == R.id.show_uploads) {
            startActivityForResult(Intent(this, UploadActivity::class.java), RC.UPLOAD_VIEWER)
            overridePendingTransition(R.anim.slide_in_bottom, android.R.anim.fade_out)
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        //        if (id == R.id.nav_info) {
        //            LogUtil.d(TAG, "안내 사항");
        //            showSnackbar("준비 중입니다.", Snackbar.LENGTH_LONG);
        //        } else if (id == R.id.nav_manual) {
        //            LogUtil.d(TAG, "사용 방법");
        //            showSnackbar("준비 중입니다.", Snackbar.LENGTH_LONG);
        //        }
        //        else if (id == R.id.nav_logout) {
        //            AuthUI.getInstance()
        //                .signOut(this)
        //                .addOnCompleteListener(new OnCompleteListener<Void>() {
        //                    public void onComplete(@NonNull Task<Void> task) {
        //                        // ...
        //                        LogUtil.d(TAG, "Successfully Logged out.");
        //
        //                        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        //                        startActivity(intent);
        //                        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);
        //                        SessionHelper.clear();
        //                        finish();
        //                    }
        //                });
        //         }
        //        else if (id == R.id.nav_notification_setting) {
        //            LogUtil.d(TAG, "측정 알림 설정");
        //            showSnackbar("준비 중입니다.", Snackbar.LENGTH_LONG);
        //        } else
        if (id == R.id.nav_bracelet_set_left) {
            index = 0
            dialog?.show()
        } else if (id == R.id.nav_bracelet_set_right) {
            index = 1
            dialog?.show()
        }
        //        else if (id == R.id.nav_withdraw) {
        //            LogUtil.d(TAG, "참여중단");
        //            showSnackbar("준비 중입니다.", Snackbar.LENGTH_LONG);
        //        }
        else if (id == R.id.nav_logout) {
            LogUtil.d(TAG, "Logged out")
            AuthUI.getInstance().signOut(this)
            FirebaseAuth.getInstance().signOut()
            SessionHelper.removeRefreshToken()
            SessionHelper.removeAccessToken()
            SessionHelper.removeUserKey()
            Realm.deleteRealm(RealmConfiguration.Builder().build())
//            Single.create<String> { emitter ->
//                emitter.onSuccess("")
//            }.subscribeOn(Schedulers.io())
//                .subscribe{ _ -> doExit() }
            doExit()
       }

        val drawer = findViewById<DrawerLayout?>(R.id.drawer_layout)
        drawer?.closeDrawer(GravityCompat.START)
        return true
    }

    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)
        LogUtil.d(TAG, "Welcome back home")
    }

    override fun onPostCreate(savedInstanceState: Bundle?) {
        super.onPostCreate(savedInstanceState)
        drawerToggle?.syncState()
    }

    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        drawerToggle?.onConfigurationChanged(newConfig)
    }

    override fun getFilterServiceUuids(): Array<UUID> {
        return SERVICE_UUIDS
    }

    override fun getScanDuration(): Long {
        return 20000L
    }

    override fun onDeviceSelected(bluetoothDevice: BluetoothDevice) {
        LogUtil.d(TAG, "Selected " + index + ": " + bluetoothDevice.address)
        dialog?.dismiss()

        if (BraceletKeeper.braceletMap[index]?.statusOb == DeviceStatus.CONNECTING) {
            showSnackbar(R.string.please_try_again_later_a_moment, Snackbar.LENGTH_SHORT)
        } else {
            sendToService(index, bluetoothDevice)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (grantResults.isNotEmpty()) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                LogUtil.i(TAG, "Permission: " + permissions[0] + " was " + grantResults[0])
                initAWSMobileClient()
                if (index != -1) {
                    dialog?.show()
                }
            } else if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                showConfirmAlertDialog("권한 설정을 하지 않으면 측정 기능을 사용할 수 없습니다.")
                if (index != -1) {
                    index = -1
                }
            }
        }
    }

    override fun onBackPressed() {
        MaterialDialog(this)
            .message(R.string.want_to_exit)
            .positiveButton(android.R.string.yes) { dialog ->
                doExit()
            }
            .negativeButton(android.R.string.no)
            .show()
    }

    override fun onDestroy() {
        super.onDestroy()
        LogUtil.d(TAG, "onDestroy()")
        doExit()
    }

    private fun doExit() {
        var disconnecting = false
//        BraceletKeeper.braceletMap.values.forEach { board ->
//            if (board.metaWearBoard != null && board.metaWearBoard?.isConnected!!) {
//                board.disposeAll()
//                board.metaWearBoard?.disconnectAsync()?.continueWith {
//                    val another = BraceletKeeper.braceletMap[if (board.index==0) 1 else 0]
//                    if (!another?.metaWearBoard?.isConnected!!) {
//                        LogUtil.e(TAG, "System.exit()")
//                        System.exit(0)
//                    }
//                }
//                disconnecting = true
//            }
//        }

        stopService(Intent(applicationContext, BraceletManagerService::class.java))
        stopService(Intent(applicationContext, SenderService::class.java))
        stopService(Intent(applicationContext, PhoneSensorService::class.java))

        if (!disconnecting) {
            LogUtil.d(TAG, "System.exit()")
            finishAffinity()
            System.exit(0)
        }
    }

    private fun showSnackbar(resourceId: Int, duration: Int) {
        val msg = resources.getString(resourceId)
        Snackbar.make(findViewById(R.id.snackbar), msg, duration).setAction("Action", null).show()
    }

    private fun showSnackbar(msg: String, duration: Int) {
        Snackbar.make(findViewById(R.id.snackbar), msg, duration).setAction("Action", null).show()
    }

    private fun showConfirmAlertDialog(msg: String) {
        MaterialDialog(this)
            .message(null, msg)
            .positiveButton(android.R.string.yes)
            .show()
    }

    private fun sendToService(index: Int, bluetoothDevice: BluetoothDevice) {
        val intent = Intent(applicationContext, BraceletManagerService::class.java)
        intent.putExtra(S.IS_BRACELET_SELECTED, true)
        intent.putExtra(S.INDEX, index)
        intent.putExtra(S.BLUETOOTH_DEVICE, bluetoothDevice)
        AppUtil.startService(this, intent)
    }

    private fun startSurveyActivity(requestCode: Int) {
        val intent = Intent(applicationContext, SurveyActivity::class.java)
        intent.putExtra(S.SURVEY_INDEX, requestCode)
        startActivityForResult(intent, requestCode)
        overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
    }

    private fun setUpDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setView(R.layout.dialog_scanner)
        dialog = builder.create()
    }

    private fun setUpActionBar() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        val drawer = findViewById<DrawerLayout>(R.id.drawer_layout)
        val toggle = ActionBarDrawerToggle(
            this, drawer, toolbar, R.string.drawer_open, R.string.drawer_close
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()

        val navigationView = findViewById<NavigationView>(R.id.nav_view)
        navigationView.setNavigationItemSelectedListener(this)
    }

    private fun initAWSMobileClient() {
        AWSMobileClient.getInstance().initialize(this) {
            LogUtil.d(TAG, "AWSMobileClient is instantiated and you are connected to AWS!")

            val intent = Intent(applicationContext, SenderService::class.java)
            intent.putExtra(S.IS_AWS_CLIENT_INITIALZIED, true)
            AppUtil.startService(this, intent)

        }.execute()
    }

    private fun isPermissionGranted(permission: String): Boolean {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!AppUtil.hasPermission(this, permission)) {
                ActivityCompat.requestPermissions(this, arrayOf(permission), 1)
                AppUtil.hasPermission(this, permission)
            } else {
                true
            }
        } else { // permission is automatically granted on sdk<23 upon installation
            true
        }
    }

    private fun onClickBraceletItem(position: Int) {
        index = position
        if (isPermissionGranted(android.Manifest.permission.ACCESS_COARSE_LOCATION)) {
            BraceletKeeper.braceletMap[index]?.let { bracelet ->
                if (bracelet.status == DeviceStatus.CONNECTED) {
                    bracelet.ledTick = 500
                    bracelet.ledDuration = 4000
                } else if (bracelet.status == DeviceStatus.DISCONNECTED) {
                    dialog?.show()
                }
            }
        }
    }

    private fun getDeviceStatusSubscriber(): Disposable {
        return Observable.merge(binding!!.btnContinue.clicks()
            , RxBus.getInstance()!!.toObservable(DeviceStatus::class.java))
            .debounce(1000L, TimeUnit.MILLISECONDS)
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap {
                if (!isPermissionGranted(android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    || !isPermissionGranted(android.Manifest.permission.ACCESS_COARSE_LOCATION)
                    || !isPermissionGranted(android.Manifest.permission.RECORD_AUDIO)) {
                    disableContinueButton()
                    Observable.empty()
                } else if(!BraceletKeeper.checkBothStatus(DeviceStatus.CONNECTED)) {
                    disableContinueButton()
                    Observable.empty()
                } else {
                    Observable.just(it)
                }
            }.observeOn(Schedulers.io())
            .flatMap {
                if (BraceletKeeper.checkBothStatus(DeviceStatus.CONNECTED)) {
                    AuthHelper.getRefreshTokenAndUserKeyMaybe().toObservable()
                        .onErrorResumeNext(Observable.empty())
                } else {
                    Observable.empty()
                }
            }.observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                enableContinueButton()
                binding!!.btnContinue.setOnClickListener {
                    startSurveyActivity(RC.SURVEY_PRE_1)
                }
            }, { e ->
                disableContinueButton()
                handleError("$TAG.getDeviceStatusSubscriber()", e)
            })
    }

    private fun getSubscriberForWebView(): Disposable {
        return ReactiveNetwork
            .observeInternetConnectivity(AppUtil.internetObservingSetting)
            .subscribeOn(Schedulers.io())
            .filter { isConnectedToInternet -> isConnectedToInternet }.flatMapMaybe {
                AuthHelper.getRefreshTokenAndUserKeyMaybe()
            }.observeOn(AndroidSchedulers.mainThread())
            .subscribe({ refreshTokenUserKeyPair ->
                val refreshToken = refreshTokenUserKeyPair.first
                binding!!.webview.loadUrl("http://dev.itchtector.co.kr:31081/?token=$refreshToken")
//                binding!!.webview.loadUrl("http://192.168.0.11:3449/?token=$refreshToken")
            }, {e -> showSnackbar("네트워크 오류", Snackbar.LENGTH_SHORT)})
    }

    private fun checkUnfinishedSession() {
        Realm.getDefaultInstance().use { realm ->
            val survey = realm.where(Survey::class.java)
                .equalTo("isSurveyClosed", false)
                .findFirst()

            if (survey != null) {
                startActivity(Intent(applicationContext, SurveyActivity::class.java)
                    .putExtra(S.SURVEY_INDEX, RC.SURVEY_POST_1)
                    .putExtra(S.SURVEY_ID, survey.id))
                overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
            }
        }
    }

    private fun enableContinueButton() {
        binding!!.btnContinue.setBackgroundResource(R.drawable.btn_round)
        binding!!.textBtnContinue.setTextColor(ResourcesCompat.getColor(resources, R.color.colorButtonEnabled, null))
    }

    private fun disableContinueButton() {
        binding!!.btnContinue.setBackgroundResource(R.drawable.btn_round_disable)
        binding!!.textBtnContinue.setTextColor(ResourcesCompat.getColor(resources, R.color.colorButtonDisabledLighter, null))
    }

    private fun handleError(tag: String, e: Throwable) {
        if (e is HttpException) {
            showSnackbar("서버에 문제가 있습니다.", Snackbar.LENGTH_SHORT)
        }
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}