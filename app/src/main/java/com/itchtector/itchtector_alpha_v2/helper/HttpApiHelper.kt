package com.itchtector.itchtector_alpha_v2.helper

import android.content.Context
import android.os.Build
import com.itchtector.itchtector_alpha_v2.api.ItchtectorServiceApi
import com.itchtector.itchtector_alpha_v2.constant.C
import com.itchtector.itchtector_alpha_v2.constant.S
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.security.cert.CertificateException
import java.util.HashMap
import java.util.concurrent.TimeUnit
import javax.net.ssl.*
import android.os.PowerManager
import com.itchtector.itchtector_alpha_v2.MyApplication
import com.itchtector.itchtector_alpha_v2.constant.DataKind
import com.itchtector.itchtector_alpha_v2.constant.RC
import com.itchtector.itchtector_alpha_v2.db.model.Survey
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.reactivex.*
import retrofit2.HttpException
import java.net.UnknownHostException

object HttpApiHelper {
//    private const val BASE_URI = "https://api.itchtector.com/"
//    private const val BASE_URI = "http://192.168.0.11:8765/"
    private const val BASE_URI = "http://dev.itchtector.co.kr:32765/"
//    private const val BASE_URI = "http://gp.itchtector.co.kr:8765/"

    private fun getUnsafeOkHttpClient(): OkHttpClient {
        try {
            // Create a trust manager that does not validate certificate chains
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                @Throws(CertificateException::class)
                override fun checkClientTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                @Throws(CertificateException::class)
                override fun checkServerTrusted(chain: Array<java.security.cert.X509Certificate>, authType: String) {
                }

                override fun getAcceptedIssuers(): Array<java.security.cert.X509Certificate> {
                    return arrayOf()
                }
            })

            // Install the all-trusting trust manager
            val sslContext = SSLContext.getInstance("SSL")
            sslContext.init(null, trustAllCerts, java.security.SecureRandom())

            // Create an ssl socket factory with our all-trusting manager
            val sslSocketFactory = sslContext.socketFactory

            val builder = OkHttpClient.Builder()
            builder.sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            builder.hostnameVerifier { hostname, session -> true }

            return builder.build()
        } catch (e: Exception) {
            throw RuntimeException(e)
        }
    }

    private fun getServiceApi(): ItchtectorServiceApi {
        val logInterceptor = HttpLoggingInterceptor()
        logInterceptor.level = HttpLoggingInterceptor.Level.BODY

        val client = OkHttpClient.Builder()
            .connectTimeout(C.TIMEOUT_CONNECTING_TO_BACKEND, TimeUnit.MILLISECONDS)
            .readTimeout(C.TIMEOUT_CONNECTING_TO_BACKEND, TimeUnit.MILLISECONDS)
            .addInterceptor(logInterceptor)
            .build()

        val retrofit = Retrofit.Builder()
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .baseUrl(BASE_URI)
            .build()

        return retrofit.create(ItchtectorServiceApi::class.java)
    }

    fun getPingMaybe(instanceId: String): Maybe<String> {
        val map = HashMap<String, Any>()
        map["instId"] = instanceId
        map["userKey"] = (SessionHelper.getUserKey() ?: "-1").toInt()
        map["platform"] = "A"

        val pkgInfo = AppUtil.getPackageInfo(MyApplication.getContext())
        val versionName = if (pkgInfo != null) ";appVer=${pkgInfo.versionName}" else ""

        val pm = MyApplication.getContext().getSystemService(Context.POWER_SERVICE) as PowerManager?
        val isScreenOn = if (pm != null) ";isScreenOn=${pm.isInteractive}" else ""

        map["extra"] = "osVer=${Build.VERSION.RELEASE}$versionName$isScreenOn"
        return getServiceApi().ping(map)
            .map { stringAnyMap ->
                val result = stringAnyMap["result"] as String
                result
            }
    }

    fun getRegisterInstanceMaybe(instanceId: String): Maybe<String> {
        val map = HashMap<String, Any>()
        map["instId"] = instanceId
        map["platform"] = "A"
        map["deviceModel"] = Build.MANUFACTURER + "|" + Build.MODEL
        return getServiceApi().registerInstance(map)
            .map { stringAnyMap ->
                val newInstanceId = stringAnyMap[S.INSTANCE_ID] as String
                PreferenceHelper.putInstanceId(newInstanceId)
                instanceId
            }
    }

    fun getLoginMaybe(instanceId: String, token: String, origin: String): Maybe<Triple<String, String, String>>  {
        val map = HashMap<String, Any>()
        map["instId"] = instanceId
        map["platform"] = "A"
        map["token"] = token
        map["origin"] = origin
        return getServiceApi().login(map)
            .map { stringAnyMap ->
                val dataMap = stringAnyMap["data"] as Map<*, *>
                val refreshToken = dataMap[S.REFRESH_TOKEN] as String
                val accessToken = dataMap[S.ACCESS_TOKEN] as String
                val userKey = dataMap[S.USER_KEY] as String
                Triple(refreshToken, accessToken, userKey)
            }
    }

    fun getNewAccessTokenMaybe(): Maybe<String> {
        val token = SessionHelper.getRefreshToken()
        return if (token != null) {
            getServiceApi().newAccessToken(token)
                .map { stringAnyMap ->
                    val dataMap = stringAnyMap["data"] as Map<*, *>
                    val accessToken = dataMap[S.ACCESS_TOKEN] as String
                    SessionHelper.putAccessToken(accessToken)
                    accessToken
                }
        } else {
            AuthHelper.getRefreshTokenAndUserKeyMaybe()
                .map { refreshTokenAndUserKeyPair ->
                    refreshTokenAndUserKeyPair.first
                }
        }
    }

    fun getSurveyPostMaybe(survey: Survey): Maybe<String> {
        val map = HashMap<String, Any>()
        map["surveyId"] = survey.id
        val entries = HashMap<String, Any>()
        for (entry in survey.entries) {
            val item = HashMap<String, Any>()
            item["v"] = entry.value!!
            item["t"] = entry.timeCreation.time
            entries[entry.key!!] = item
        }
        map["entries"] = entries

        val isSurveyComplete = RC.SURVEY_POST_2.toString() in entries.keys

        return AuthHelper.getAccessTokenMaybe()
            .flatMap { token ->
                getServiceApi().sendSurveyAnswer(token, map)
            }.map { stringAnyMap ->
                val surveyId = stringAnyMap[S.SURVEY_ID] as String
                surveyId
            }.retry { n, e ->
                if (!isSurveyComplete && e is UnknownHostException) {
                    false
                } else if (e is HttpException && e.code() == 403) {
                    SessionHelper.removeAccessToken()
                    AppUtil.sleep(2000)
                    true
                } else {
                    AppUtil.sleep(10000)
                    true
                }
            }.onErrorResumeNext(Maybe.empty())
    }

    fun getMetadataPostMaybe(fileStateId: String?,
                             surveyId: String?,
                             userKey: String?,
                             dirPath: String?,
                             filename: String?,
                             kind: String?): Maybe<String> {
        val map = HashMap<String, Any>()
        map["surveyId"] = surveyId ?: ""
        map["userKey"] = userKey ?: ""
        map["dirPath"] = dirPath ?: ""
        map["filename"] = filename ?: ""
        map["kind"] = kind ?: ""
        return AuthHelper.getAccessTokenMaybe()
            .flatMap { token ->
                getServiceApi().sendFileMetadata(token, map)
            }.map { fileStateId!! }
            .retry { n, e ->
                LogUtil.e("getMetadataPostMaybe()", "${e.message}")
                if (e is HttpException && e.code() == 403) {
                    SessionHelper.removeAccessToken()
                    AppUtil.sleep(2000)
                    true
                } else {
                    AppUtil.sleep(10000)
                    true
                }
            }.onErrorResumeNext(Maybe.empty())
    }

    fun getFileTxCompletePostMaybe(surveyId: String?): Maybe<String> {
        val map = HashMap<String, Any>()
        map["surveyId"] = surveyId ?: ""
        return AuthHelper.getAccessTokenMaybe()
            .flatMap { token ->
                getServiceApi().sendFileTxComplete(token, map)
            }.map { surveyId!! }
            .retry { n, e ->
                if (e is HttpException && e.code() == 403) {
                    SessionHelper.removeAccessToken()
                    AppUtil.sleep(2000)
                    true
                } else {
                    AppUtil.sleep(10000)
                    true
                }
            }.onErrorResumeNext(Maybe.empty())
    }
}