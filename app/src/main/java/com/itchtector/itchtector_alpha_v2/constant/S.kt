package com.itchtector.itchtector_alpha_v2.constant

object S {
    const val S3_BUCKET_SENSOR_DATA = "itchtector-sensordata"
    const val SESSION_PREF_NAME = "session"
    const val INSTANCE_ID = "instanceId"
    const val SURVEY_INDEX = "surveyIndex"
    const val SURVEY_ID = "surveyId"
    const val INDEX = "index"
    const val IS_AWS_CLIENT_INITIALZIED = "isAWSClientInitialzied"
    const val IS_START_BUTTON_PRESSED = "isStartButtonPressed"
    const val IS_STOP_BUTTON_PRESSED = "isStopButtonPressed"
    const val IS_BRACELET_SELECTED = "isBraceletSelected"
    const val REFRESH_TOKEN = "refreshToken"
    const val ACCESS_TOKEN = "accessToken"
    const val USER_KEY = "userKey"
    const val BLUETOOTH_DEVICE = "blutoothDevice"
}