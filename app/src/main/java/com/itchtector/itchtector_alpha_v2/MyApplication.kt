package com.itchtector.itchtector_alpha_v2

import android.app.Application
import android.content.Context
import android.content.Intent
import com.itchtector.itchtector_alpha_v2.db.DBMigration
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import io.realm.Realm
import io.realm.RealmConfiguration
import java.security.Security


class MyApplication : Application() {

    companion object {
        private var application: Application? = null

        fun getContext(): Context {
            return application?.applicationContext!!
        }
    }

    override fun onCreate() {
        super.onCreate()
        application = this

        System.setProperty("networkaddress.cache.ttl", "0")
        Security.setProperty("networkaddress.cache.ttl", "0")

        Realm.init(this)
        val config = RealmConfiguration.Builder()
            .schemaVersion(1)
            .migration(DBMigration())
            .build()
        Realm.setDefaultConfiguration(config)

        AppUtil.startService(this, Intent(applicationContext, BraceletManagerService::class.java))
        AppUtil.startService(this, Intent(applicationContext, SenderService::class.java))
    }
}