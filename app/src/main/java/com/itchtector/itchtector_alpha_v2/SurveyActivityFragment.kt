package com.itchtector.itchtector_alpha_v2

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.Crashlytics
import com.itchtector.itchtector_alpha_v2.adapter.ChecklistAdapter
import com.itchtector.itchtector_alpha_v2.bus.RxBus
import com.itchtector.itchtector_alpha_v2.bus.message.UnsentSurvey
import com.itchtector.itchtector_alpha_v2.constant.RC
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.databinding.ContentSurveyPost1Binding
import com.itchtector.itchtector_alpha_v2.databinding.ContentSurveyPost2Binding
import com.itchtector.itchtector_alpha_v2.databinding.ContentSurveyPre1Binding
import com.itchtector.itchtector_alpha_v2.databinding.ContentSurveyPre2Binding
import com.itchtector.itchtector_alpha_v2.db.model.Survey
import com.itchtector.itchtector_alpha_v2.db.model.SurveyEntry
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import com.itchtector.itchtector_alpha_v2.viewmodel.ChecklistItem
import com.itchtector.itchtector_alpha_v2.viewmodel.NumberPickerViewModel
import com.itchtector.itchtector_alpha_v2.viewmodel.SwitchViewModel
import io.realm.Realm

class SurveyActivityFragment : Fragment() {
    private var TAG: String = ""
    private var surveyIndex: Int = 0
    private var surveyId: String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        surveyIndex = arguments!!.getInt(S.SURVEY_INDEX)
        surveyId = arguments!!.getString(S.SURVEY_ID)
        TAG = "SurveyActivityFragment $surveyIndex"
        LogUtil.v(TAG, "$surveyIndex - $surveyId")
        val resourceId = when (surveyIndex) {
            RC.SURVEY_PRE_1 -> R.layout.content_survey_pre_1
            RC.SURVEY_PRE_2 -> R.layout.content_survey_pre_2
            RC.SURVEY_POST_1 -> R.layout.content_survey_post_1
            RC.SURVEY_POST_2 -> R.layout.content_survey_post_2
            else -> 0
        }

        var view: View? = null

        when (surveyIndex) {
            RC.SURVEY_PRE_1 -> {
                val binding = DataBindingUtil.inflate<ContentSurveyPre1Binding>(
                    inflater,
                    resourceId, container, false
                )
                binding.setVariable(BR.item, NumberPickerViewModel(0, 0, 10))
                binding.executePendingBindings()
                binding.numberPicker.wrapSelectorWheel = false
                binding.btnContinue.setOnClickListener { v ->
                    val surveyEntry =
                        SurveyEntry(RC.SURVEY_PRE_1.toString()
                            , binding.numberPicker.value.toString())
                    val survey = addSurveyEntryToSurvey(surveyEntry)
                    surveyId = survey.id
                    sendSurvey(survey)

                    val bundle = Bundle()
                    bundle.putInt(S.SURVEY_INDEX, RC.SURVEY_PRE_2)
                    bundle.putString(S.SURVEY_ID, surveyId)
                    val surveyActivityFragment = SurveyActivityFragment()
                    surveyActivityFragment.arguments = bundle

                    fragmentManager!!.beginTransaction()
                        .setCustomAnimations(
                            R.anim.slide_in_right, R.anim.slide_out_left,
                            R.anim.slide_in_left, R.anim.slide_out_right
                        )
                        .replace(R.id.container, surveyActivityFragment)
                        .addToBackStack(null)
                        .commit()
                }
                view = binding.root
            }
            RC.SURVEY_PRE_2 -> {
                val binding = DataBindingUtil.inflate<ContentSurveyPre2Binding>(
                    inflater,
                    resourceId, container, false
                )
                binding.setVariable(BR.item, SwitchViewModel(false))
                binding.btnContinue.setOnClickListener { v ->

                    val surveyEntry = SurveyEntry(RC.SURVEY_PRE_2.toString()
                        , if (binding!!.switch1.isChecked) "yes" else "no")
                    val survey = addSurveyEntryToSurvey(surveyEntry)
                    sendSurvey(survey)

                    val intent = Intent(context, RecorderActivity::class.java)
                    intent.putExtra(S.SURVEY_ID, surveyId)
                    startActivityForResult(intent, RC.RECORDER)
                    activity?.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left)
                }
                view = binding.root
            }
            RC.SURVEY_POST_1 -> {
                val binding = DataBindingUtil.inflate<ContentSurveyPost1Binding>(
                    inflater,
                    resourceId, container, false
                )
                binding.setVariable(BR.item, NumberPickerViewModel(0, 0, 10))
                binding.executePendingBindings()
                binding.numberPicker.wrapSelectorWheel = false
                binding.btnContinue.setOnClickListener { v ->
                    val surveyEntry =
                        SurveyEntry(RC.SURVEY_POST_1.toString()
                            , binding.numberPicker.value.toString())
                    val survey = addSurveyEntryToSurvey(surveyEntry)
                    sendSurvey(survey)

                    val bundle = Bundle()
                    bundle.putInt(S.SURVEY_INDEX, RC.SURVEY_POST_2)
                    bundle.putString(S.SURVEY_ID, surveyId)
                    val surveyActivityFragment = SurveyActivityFragment()
                    surveyActivityFragment.arguments = bundle
                    fragmentManager!!.beginTransaction()
                        .setCustomAnimations(
                            R.anim.slide_in_right, R.anim.slide_out_left,
                            R.anim.slide_in_left, R.anim.slide_out_right
                        )
                        .replace(R.id.container, surveyActivityFragment)
                        .addToBackStack(null)
                        .commit()
                }
                view = binding.root
            }
            RC.SURVEY_POST_2 -> {
                val binding = DataBindingUtil.inflate<ContentSurveyPost2Binding>(
                    inflater,
                    resourceId, container, false
                )
                val list = ArrayList<ChecklistItem>()
                list.add(
                    ChecklistItem(
                        "팔찌를 벗고 다시 착용한 적이 있어요.",
                        "HaveTakenOff",
                        0,
                        false,
                        false
                    )
                )
                list.add(
                    ChecklistItem(
                        "자다 깨서 5분 이상 활동했어요.",
                        "Awaken",
                        0,
                        false,
                        false
                    )
                )
                list.add(
                    ChecklistItem(
                        "팔찌에 닿는 부분이 가려워요.",
                        "WristItches",
                        0,
                        false,
                        false
                    )
                )
                list.add(ChecklistItem("", "", 0, true, false, "직접 입력"))
                list.add(
                    ChecklistItem(
                        "성공적으로",
                        "Successful",
                        1,
                        false,
                        true
                    )
                )
                binding.checklist = list
                binding.checklistView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)
                binding.checklistView.adapter = ChecklistAdapter(activity as SurveyActivity)
                binding.btnContinue.setOnClickListener { v ->
                    val arrList = ArrayList<String>()
                    list.forEach { it ->
                        if (it.checked) {
                            val str = if (it.abbreviation != "") it.abbreviation else it.description
                            arrList.add(str)
                        }
                    }
                    val joined = AppUtil.stringJoin("|", arrList.toArray()!!, "")
                    val surveyEntry =
                        SurveyEntry(RC.SURVEY_POST_2.toString()
                            , joined)
                    val survey = addSurveyEntryToSurvey(surveyEntry)
                    sendSurvey(survey)

                    Realm.getDefaultInstance().use { realm ->
                        val survey = realm.where(Survey::class.java)
                            .equalTo("id", surveyId!!)
                            .findFirst()

                        realm.beginTransaction()
                        survey.isSurveyClosed = true
                        realm.commitTransaction()
                    }

                    val intent = Intent(context, MainActivity::class.java)
                    intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP
                    startActivity(intent)
                }
                view = binding.root
                view.setOnClickListener { AppUtil.hideKeyboard(this.activity as AppCompatActivity) }
            }
        }

        setUpActionBar()
        return view
    }

    private fun setUpActionBar() {
        val activity = (this.activity as AppCompatActivity)
        val toolbar = activity.findViewById<Toolbar>(R.id.toolbar)
        activity.setSupportActionBar(toolbar)

        toolbar.setNavigationIcon(R.mipmap.icon_back)
        activity.supportActionBar?.setDisplayHomeAsUpEnabled(surveyIndex != RC.SURVEY_POST_1)
        activity.supportActionBar?.setHomeButtonEnabled(surveyIndex != RC.SURVEY_POST_1)

        var title = ""
        if (surveyIndex == RC.SURVEY_PRE_1 || surveyIndex == RC.SURVEY_PRE_2) {
            title = "자기 전 기록"
        } else if (surveyIndex == RC.SURVEY_POST_1 || surveyIndex == RC.SURVEY_POST_2) {
            title = "일어난 후 기록"
        }
        activity.supportActionBar?.title = title
    }

    private fun addSurveyEntryToSurvey(surveyEntry: SurveyEntry): Survey {
        return Realm.getDefaultInstance().use { realm ->
            LogUtil.d(TAG, "surveyEntry: ${surveyEntry.key} ${surveyEntry.value}")
            realm.beginTransaction()
            val survey: Survey? = if (surveyId == null) {
                realm.createObject(Survey::class.java, AppUtil.uuid())
            } else {
                realm.where(Survey::class.java).equalTo("id", surveyId).findFirst()
            }
            survey!!.addEntry(surveyEntry)
            survey.isSent = false
            realm.commitTransaction()
            realm.copyFromRealm(survey, 1)
        }
    }

    @SuppressLint("CheckResult")
    private fun sendSurvey(survey: Survey) {
        RxBus.getInstance()!!.send(UnsentSurvey(survey.id))
    }

    private fun handleError(tag: String, e: Throwable) {
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}