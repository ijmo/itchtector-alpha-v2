package com.itchtector.itchtector_alpha_v2

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.graphics.Color
import android.os.IBinder
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.db.model.Bracelet
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import com.mbientlab.metawear.android.BtleService
import io.reactivex.disposables.CompositeDisposable
import io.realm.Realm
import android.os.Build
import androidx.core.app.NotificationCompat


class BraceletManagerService : Service(), ServiceConnection {
    private val TAG = BraceletManagerService::class.simpleName!!
    private val compositeDisposable = CompositeDisposable()
    private var serviceBinder: BtleService.LocalBinder? = null
    private var isBound: Boolean = false
    private var isRunning: Boolean = false

    override fun onCreate() {
        super.onCreate()
        bindService()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            when {
                intent.getBooleanExtra(S.IS_START_BUTTON_PRESSED, false) -> {
                    val surveyId = intent.getStringExtra(S.SURVEY_ID)
                    BraceletKeeper.braceletMap.values.forEach { bracelet -> bracelet.startTracking(surveyId) }
                    isRunning = true
                }
                intent.getBooleanExtra(S.IS_STOP_BUTTON_PRESSED, false) -> {
                    BraceletKeeper.braceletMap.values.forEach(Bracelet::stopTracking)
                    compositeDisposable.clear()
                    isRunning = false
                }
                intent.getBooleanExtra(S.IS_BRACELET_SELECTED, false) -> {
                    val index = intent.getIntExtra(S.INDEX, -1)
                    val bluetoothDevice = intent.getParcelableExtra(S.BLUETOOTH_DEVICE) as BluetoothDevice?
                    val metaWearBoard = serviceBinder?.getMetaWearBoard(bluetoothDevice)
                    val bracelet: Bracelet?
                    if (metaWearBoard == null) {
                        bracelet = Bracelet(index)
                        bracelet.macAddress = bluetoothDevice!!.address
                    } else {
                        bracelet = Bracelet(index, metaWearBoard)
                        BraceletKeeper.setBracelet(index, bracelet).connect()
                    }

                    Realm.getDefaultInstance().use { realm ->
                        realm.beginTransaction()
                        realm.where(Bracelet::class.java).equalTo("index", index).findFirst()?.deleteFromRealm()
                        realm.copyToRealm(bracelet)
                        realm.commitTransaction()
                    }
                }
            }
        }

        val channelId = "itchtector.braceletManagerService"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val chan = NotificationChannel(channelId,
                "이치텍터 센서 통신 서비스", NotificationManager.IMPORTANCE_NONE)
            chan.lightColor = Color.BLUE
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            service.createNotificationChannel(chan)

            val builder = Notification.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true)

            val notification = builder.build()
            startForeground(1, notification)
        } else {
            val builder = NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)

            val notification = builder.build()

            startForeground(1, notification)
        }
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        compositeDisposable.dispose()
        unbindService()
    }

    override fun onBind(intent: Intent?): IBinder? {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onServiceDisconnected(name: ComponentName?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
        serviceBinder = service as BtleService.LocalBinder
        setUpKnownBracelets()
    }

    private fun bindService() {
        isBound = applicationContext.bindService(
            Intent(this, BtleService::class.java),
            this, Context.BIND_AUTO_CREATE
        )
    }

    private fun unbindService() {
        if (isBound) {
            applicationContext.unbindService(this)
            isBound = false
            serviceBinder = null
        }
    }

    private fun setUpKnownBracelets() {
        Realm.getDefaultInstance().use { realm ->
            val bluetoothManager = getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
            for (i in 0 until BraceletKeeper.NUM_DEVICES) {
                val savedBracelet =
                    realm.where(Bracelet::class.java).equalTo("index", i).findFirst() ?: continue
                val bluetoothDevice =
                    bluetoothManager.adapter?.getRemoteDevice(savedBracelet.macAddress) ?: continue
                val metaWearBoard = serviceBinder?.getMetaWearBoard(bluetoothDevice)
                val bracelet = Bracelet(i, metaWearBoard!!)
                BraceletKeeper.setBracelet(i, bracelet).connect()
                LogUtil.d(TAG, "Known device: ${bracelet.macAddress} to ${bracelet.getLR()}")
            }
        }
    }
}