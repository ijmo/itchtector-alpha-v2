package com.itchtector.itchtector_alpha_v2.constant

enum class DeviceStatus {
    CONNECTING, CONNECTED, DISCONNECTING, DISCONNECTED, RUNNING
}