package com.itchtector.itchtector_alpha_v2.adapter

import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.itchtector.itchtector_alpha_v2.viewmodel.ChecklistItem
import com.itchtector.itchtector_alpha_v2.R
import com.itchtector.itchtector_alpha_v2.SurveyActivity
import com.itchtector.itchtector_alpha_v2.databinding.SurveyChecklistItemBinding
import com.itchtector.itchtector_alpha_v2.util.AppUtil

class ChecklistAdapter(val activity: SurveyActivity) : RecyclerView.Adapter<ChecklistAdapter.ViewHolder>() {
    var checklist: ArrayList<ChecklistItem> = ArrayList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    var itemListener: (Int, ChecklistItem) -> Unit = { _, _ -> }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChecklistAdapter.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context),
            R.layout.survey_checklist_item, parent, false
        )
        return ViewHolder(binding as SurveyChecklistItemBinding)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        val item = checklist[position]
        viewHolder.bind(position, item, itemListener)
    }

    override fun getItemCount(): Int {
        return checklist.size
    }

    inner class ViewHolder(var binding: SurveyChecklistItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(position: Int, item: ChecklistItem, itemListener: (Int, ChecklistItem) -> Unit) {
            binding.item = item
//            LogUtil.d("ChecklistAdapter.ViewHolder", "bind() - $position, ${item.group}")

            val layoutParams =
                LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)
            if (position > 0 && checklist[position - 1].group != item.group) {
                layoutParams.setMargins(0, 40, 0, 0)
            } else {
                layoutParams.setMargins(0, 0, 0, 0)
            }
            binding.wrapper.layoutParams = layoutParams
            if (item.group == 0) {
                binding.checkBox.setBackgroundResource(R.drawable.seletor_check_red)
            } else {
                binding.checkBox.setBackgroundResource(R.drawable.seletor_check_green)
            }

            if (!item.isEditable) {
                binding.editItem.setOnClickListener {
//                    LogUtil.d("editItem", "focusable=${binding.editItem.isFocusable} clickable=${binding.editItem.isClickable}")
                    item.checked = !item.checked
                    excludeOtherGroups(item.group)
                    notifyDataSetChanged()
                }

                binding.root.setOnClickListener {
//                    LogUtil.d("root", "focusable=${binding.editItem.isFocusable} clickable=${binding.editItem.isClickable}")
                    item.checked = !item.checked
                    excludeOtherGroups(item.group)
                    notifyDataSetChanged()
                }
            } else {
                binding.root.setOnClickListener {
                    binding.editItem.requestFocus()
                }

                binding.editItem.setOnFocusChangeListener { v, hasFocus ->
//                    LogUtil.d("OnFocusChangeListener", "hasFocus=$hasFocus")
                    if (hasFocus) {
                        binding.editItem.hint = ""
                    } else {
                        val text = binding.editItem.text.toString().trim()
                        if (text == "") {
                            binding.editItem.hint = item.hint
                        }
                    }
                }

                binding.editItem.setOnEditorActionListener { v, actionId, event ->
                    if (actionId == EditorInfo.IME_ACTION_SEARCH ||
                        actionId == EditorInfo.IME_ACTION_DONE ||
                        event != null
                        && event.action == KeyEvent.ACTION_DOWN
                        && event.keyCode == KeyEvent.KEYCODE_ENTER) {
                        if (event == null || !event.isShiftPressed) {
                            val text = binding.editItem.text.toString().trim()
                            if (text == "") {
                                item.description = ""
                                item.checked = false
                            } else {
                                item.description = text
                                item.checked = true
                                excludeOtherGroups(item.group)
                            }
                            AppUtil.hideKeyboard(activity)
                            binding.dummy.requestFocus()
                            notifyDataSetChanged()
                            true
                        }
                    }
                    false
                }
            }

            binding.editItem.hint = item.hint
            binding.editItem.isFocusableInTouchMode = item.isEditable
            binding.editItem.isFocusable = item.isEditable
            binding.editItem.isClickable = item.isEditable
            binding.checkBox.isClickable = false
        }
    }

    private fun excludeOtherGroups(group: Int) {
        checklist.forEach { it ->
            if (it.group != group) {
                it.checked = false
            }
        }
    }
}