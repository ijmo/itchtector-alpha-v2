package com.itchtector.itchtector_alpha_v2.helper

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.google.gson.Gson
import com.itchtector.itchtector_alpha_v2.MyApplication
import com.itchtector.itchtector_alpha_v2.constant.S


object PreferenceHelper {
    private val TAG = PreferenceHelper::class.java.simpleName

    fun getDefaultPreferences(): SharedPreferences? {
        return PreferenceManager.getDefaultSharedPreferences(MyApplication.getContext())
    }

    fun getPreferences(name: String): SharedPreferences? {
        return MyApplication.getContext().getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    fun getPreferencesSession(): SharedPreferences? {
        return MyApplication.getContext().getSharedPreferences(S.SESSION_PREF_NAME, Context.MODE_PRIVATE)
    }

    fun putPreferenceKV(editor: SharedPreferences.Editor, key: String, value: Any) {
        val gson = Gson()
        val json = gson.toJson(value)
        editor.putString(key, json)
        editor.apply()
    }

    fun putKeyValueDefault(key: String, value: Any) {
        val editor = getDefaultPreferences()!!.edit()
        putPreferenceKV(editor, key, value)
    }

    fun putKeyValueDefault(key: String, value: String) {
        val editor = getDefaultPreferences()!!.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun removeKeyDefault(key: String) {
        val editor = getDefaultPreferences()!!.edit()
        editor.remove(key)
        editor.apply()
    }

    fun putKeyValue(key: String, value: Any, prefName: String) {
        val editor = getPreferences(prefName)!!.edit()
        putPreferenceKV(editor, key, value)
    }

    fun putKeyValue(key: String, value: String, prefName: String) {
        val editor = getPreferences(prefName)!!.edit()
        editor.putString(key, value)
        editor.apply()
    }

    fun removeKeyValue(key: String, prefName: String) {
        val editor = getPreferences(prefName)!!.edit()
        editor.remove(key)
        editor.commit()
    }

    fun getPreferenceValue(preferences: SharedPreferences, key: String): Any {
        val gson = Gson()
        val json = preferences.getString(key, null)
        return gson.fromJson(json, Any::class.java)
    }

    fun getValueDefault(key: String): Any {
        val preferences = getDefaultPreferences()
        return getPreferenceValue(preferences!!, key)
    }

    fun getStringValueDefault(key: String): String? {
        val preferences = getDefaultPreferences()
        return preferences!!.getString(key, null)
    }

    fun getValue(key: String, prefName: String): Any {
        val preferences = getPreferences(prefName)
        return getPreferenceValue(preferences!!, key)
    }

    fun getStringValue(key: String, prefName: String): String? {
        val preferences = getPreferences(prefName)
        return preferences!!.getString(key, null)
    }

    fun clearDefaultPreferences() {
        val editor = getDefaultPreferences()!!.edit()
        editor.clear()
        editor.apply()
    }

    fun checkInstanceId(): Boolean {
        // Check Instance Id
        val instId = getStringValueDefault(S.INSTANCE_ID)
        if (instId == null) {
            return false
        } else if (instId.trim { it <= ' ' } == "") {
            removeKeyDefault(S.INSTANCE_ID)
            return false
        }

        return true
    }

    fun putInstanceId(instanceId: String) {
        putKeyValueDefault(S.INSTANCE_ID, instanceId)
    }

    fun getInstanceId(): String? {
        return getStringValueDefault(S.INSTANCE_ID)
    }
}