package com.itchtector.itchtector_alpha_v2.constant

object C {
    const val INDEX_PHONE_SENSOR = 10
    const val INDEX_PHONE_MIC = 10

    const val DEVICE_LOOPER_INTERVAL: Long = 500L
    const val TICK_BATTERY_CHECK: Long = 20000L
    const val TICK_PLAY_LED: Long = 500L
    const val TICK_PLAY_LED_HEARTBEAT: Long = 4000L

    const val INTERVAL_CHECK_INTERNET: Int = 5000
    const val INTERVAL_LOOPER_PING: Long = 120000L
    const val INTERVAL_POLLING_CHART: Long = 60000L

    const val TIMEOUT_CONNECTING_TO_BACKEND: Long = 7000L
    const val TIMEOUT_AWAKE = 24*60*60*1000L

    const val DATA_PROCESSOR_TIMESPAN: Long = 500L
    const val INTERVAL_CHECK_SOUND_FILE_SIZE = 4000L
    const val STOPWATCH_TIMESPAN: Long = 200L

    const val MAX_FILE_SIZE: Long = 1024 * 1024 * 10L
}