package com.itchtector.itchtector_alpha_v2.viewmodel

data class NumberPickerViewModel(val num: Int, val min: Int, val max: Int)