package com.itchtector.itchtector_alpha_v2.constant

enum class DataKind {
    META_MOTION_R, PHONE_SENSOR, PHONE_MIC, LOCATION
}