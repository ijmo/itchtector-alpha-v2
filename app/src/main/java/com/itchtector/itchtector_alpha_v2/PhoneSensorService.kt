package com.itchtector.itchtector_alpha_v2

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener2
import android.hardware.SensorManager
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.crashlytics.android.Crashlytics
import com.itchtector.itchtector_alpha_v2.bus.RxBus
import com.itchtector.itchtector_alpha_v2.bus.message.NewFile
import com.itchtector.itchtector_alpha_v2.bus.message.RawData
import com.itchtector.itchtector_alpha_v2.constant.C
import com.itchtector.itchtector_alpha_v2.constant.DataKind
import com.itchtector.itchtector_alpha_v2.constant.S
import com.itchtector.itchtector_alpha_v2.db.model.FileState
import com.itchtector.itchtector_alpha_v2.helper.SessionHelper
import com.itchtector.itchtector_alpha_v2.util.AppUtil
import com.itchtector.itchtector_alpha_v2.util.DateUtil
import com.itchtector.itchtector_alpha_v2.util.LogUtil
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import io.realm.Realm
import java.util.concurrent.TimeUnit

class PhoneSensorService(name: String? = "PhoneSensorService") : IntentService(name), SensorEventListener2 {

    private val TAG = PhoneSensorService::class.simpleName!!
    private val compositeDisposable = CompositeDisposable()
    private var sensorManager: SensorManager? = null
    private var linearAccelSensor: Sensor? = null
    private var isRunning: Boolean = false
    private var index = C.INDEX_PHONE_SENSOR
    private var currentFileState: FileState? = null
    private var surveyId: String? = null

    override fun onCreate() {
        super.onCreate()
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        linearAccelSensor = sensorManager?.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION)
    }

    override fun onDestroy() {
        super.onDestroy()
    }

    override fun onBind(intent: Intent?): IBinder? {
        return super.onBind(intent)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            when {
                intent.getBooleanExtra(S.IS_START_BUTTON_PRESSED, false) -> {
                    surveyId = intent.getStringExtra(S.SURVEY_ID)
                    isRunning = true
                    startSensing()
                }
                intent.getBooleanExtra(S.IS_STOP_BUTTON_PRESSED, false) -> {
                    isRunning = false
                    stopSensing()
                }
            }
        }

        val channelId = "itchtector.phoneSensorService"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val chan = NotificationChannel(channelId,
                "이치텍터 센서 통신 서비스", NotificationManager.IMPORTANCE_NONE)
            chan.lightColor = Color.BLUE
            chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
            val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            service.createNotificationChannel(chan)

            val builder = Notification.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setAutoCancel(true)

            val notification = builder.build()
            startForeground(1, notification)
        } else {
            val builder = NotificationCompat.Builder(this, channelId)
                .setContentTitle(getString(R.string.app_name))
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setAutoCancel(true)

            val notification = builder.build()

            startForeground(1, notification)
        }

        return Service.START_STICKY
    }

    override fun onHandleIntent(intent: Intent?) {
        LogUtil.d(TAG, "onHandleIntent")
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        LogUtil.d(TAG, "accuracy changed: $accuracy")
    }

    override fun onFlushCompleted(sensor: Sensor?) {
        LogUtil.d(TAG, "onFlushCompleted")
    }

    override fun onSensorChanged(event: SensorEvent?) {
        when (event!!.sensor?.type) {
            Sensor.TYPE_LINEAR_ACCELERATION -> {
                val subject = "LI"
                val objects =
                    arrayOf("LI",
                        event.values[0],
                        event.values[1],
                        event.values[2],
                        0,
                        0,
                        System.currentTimeMillis(),
                        event.accuracy)
                val row = AppUtil.stringJoin(",", objects, "\n")
                RxBus.getInstance()!!.send(RawData(index, subject, row))
            }
        }
    }

    private fun startSensing() {
        sensorManager?.registerListener(this, linearAccelSensor, 20000, 500000)
        compositeDisposable.add(getDataSubscriber())
    }

    private fun stopSensing() {
        sensorManager?.unregisterListener(this)
        compositeDisposable.clear()

        if (currentFileState != null && currentFileState?.size!! > 0) {
            currentFileState?.closeFileOutputStreams()
            sendFile(currentFileState?.id!!)
            currentFileState = null
        }
    }

    private fun getDataSubscriber(): Disposable {
        return RxBus.getInstance()!!.toObservable(RawData::class.java)
            .filter { rawData -> rawData.index == index }
            .buffer(C.DATA_PROCESSOR_TIMESPAN, TimeUnit.MILLISECONDS)
            .subscribeOn(Schedulers.newThread())
            .map { arr: List<RawData> ->
                if (arr.isEmpty()) {
                    "[P] Passing: 0 row"
                } else {
                    prepareFileState()

                    if (currentFileState == null) {
                        throw Throwable("currentFileState is null")
                    }

                    if (currentFileState?.isWritable()!!) {
                        val chunk = AppUtil.stringJoin("", arr.map { it.message }.toTypedArray(), "")
                        val chunkBytes = chunk.toByteArray()
                        val sizeGyro = arr.filter { it.kind == "GY" }.size
                        val sizeOri = arr.filter { it.kind == "OR" }.size
                        val sizeLin = arr.filter { it.kind == "LI" }.size
                        currentFileState?.write(chunkBytes)
                        "[P] Recv: ${arr.size}($sizeGyro,$sizeOri,$sizeLin) during ${C.DATA_PROCESSOR_TIMESPAN}ms --> ${currentFileState?.filename}"
                    } else {
                        throw Throwable("file is not writable")
                    }
                }
            }.subscribe({chunk -> LogUtil.v(TAG, chunk)}, {e -> handleError(e)})
    }

    @Synchronized private fun prepareFileState() {
        if (currentFileState != null && currentFileState?.size!! > C.MAX_FILE_SIZE) {
            currentFileState?.closeFileOutputStreams()
            sendFile(currentFileState?.id!!)
            currentFileState = null
        }

        if (currentFileState == null || !currentFileState?.isWritable()!!) {
            val userKey= SessionHelper.getUserKey()!!
            val dirPath = userKey + "/" + DateUtil.getStringDateFormat("yyyy/MM/dd")
            val filename = DateUtil.getStringDateFormat("yyyyMMdd_HHmmss_SSS") + "_PS.csv"

            val newFileState = FileState(DataKind.PHONE_SENSOR.toString(), index, surveyId!!, userKey, dirPath, filename)

            if (newFileState.makeDir()) {
                newFileState.openFileOutputStreams()
                currentFileState = newFileState
            } else {
                newFileState.closeFileOutputStreams()
            }

            try {
                Realm.getDefaultInstance().use { realm ->
                    realm.beginTransaction()
                    realm.copyToRealm(newFileState)
                    realm.commitTransaction()
                }
            } catch (e: Throwable) {
                handleError(e)
            }
        }
    }

    private fun sendFile(fileStateId: String) {
        RxBus.getInstance()!!.send(NewFile(fileStateId))
    }

    private fun handleError(tag: String, e: Throwable) {
        var msg = e.message?.replace(",", ".")
        msg = msg ?: e.toString()
        val objects = arrayOf(
            "ER",
            e.javaClass.simpleName,
            msg,
            0,
            0,
            0,
            DateUtil.getTimeNow().time.toString(),
            0
        )
        val row = AppUtil.stringJoin(",", objects, "\n")
        RxBus.getInstance()?.send(RawData(index, "", row))
        Crashlytics.logException(e)
        LogUtil.e(tag, e)
    }

    private fun handleError(e: Throwable) {
        handleError(TAG, e)
    }
}